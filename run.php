<?php declare(strict_types=1);

/**
 * Test script for email parser.
 * 
 * This file will give the email structure as object tree form.
 * 
 * Usage:
 * php test.php emailAddress "<email>"
 * php test.php emailAddressList "<email list>"
 * php test.php mailbox "<mailbox>"
 * php test.php mailboxList "<mailbox list>"
 * php test.php mailboxGroup "<mailbox group>"
 * php test.php mailboxGroupList "<mailbox group list>"
 * 
 * @author Anastaszor
 */

global $argv;

if(!isset($argv[1]))
	throw new InvalidArgumentException('The first argument should be the method to parse the mail data.');
$method = $argv[1];
$allowed = ['emailAddress', 'emailAddressList', 'mailbox', 'mailboxList', 'mailboxGroup', 'mailboxGroupList'];
if(!in_array($method, $allowed))
	throw new InvalidArgumentException('The method to parse does not exists, use one of the following : '.implode(', ', $allowed));

if(!isset($argv[2]))
	throw new InvalidArgumentException('The second argument should be the email data to parse.');
$data = $argv[2];

echo "\n\t".'Input : '.$data."\n\n";

$composer = __DIR__.'/vendor/autoload.php';
if(!is_file($composer))
	throw new RuntimeException('You should run composer first.');
require $composer;

$parserClass = 'PhpExtended\\Email\\'.ucfirst($method).'Parser';
$parser = $parserClass::get();
$parsed = $parser->parseString($data);
var_dump($parsed);
