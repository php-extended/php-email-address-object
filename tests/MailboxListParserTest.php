<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\Email\EmailAddress;
use PhpExtended\Email\Mailbox;
use PhpExtended\Email\MailboxList;
use PhpExtended\Email\MailboxListParser;
use PHPUnit\Framework\TestCase;

/**
 * MailboxListParser class file.
 *
 * @author Anastaszor
 * @covers \PhpExtended\Email\MailboxListParser
 *
 * @internal
 *
 * @small
 */
class MailboxListParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 *
	 * @var MailboxListParser
	 */
	protected MailboxListParser $_object;
	
	public function testParseEmptyString() : void
	{
		$this->assertEquals(new MailboxList([]), $this->_object->parse(null));
	}
	
	public function testParseEmailAddress() : void
	{
		$expected = new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com']))),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('foo@example.com'));
	}
	
	public function testParseAngularEmailAddress() : void
	{
		$expected = new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com']))),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('<foo@example.com>'));
	}
	
	public function testParseAngularIncompleteEmailAddress() : void
	{
		$expected = new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com']))),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('<foo@example.com'));
	}
	
	public function testParseMailbox() : void
	{
		$expected = new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc'),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('"Foo, Inc" <foo@example.com>'));
	}
	
	public function testParseUnquotedMailbox() : void
	{
		$expected = new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo Inc'),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('Foo Inc <foo@example.com>'));
	}
	
	public function testParseAngularIncompleteMailbox() : void
	{
		$expected = new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc'),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('"Foo, Inc" <foo@example.com'));
	}
	
	public function testParseMultipleEmailAddresses() : void
	{
		$expected = new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com']))),
			new Mailbox(new EmailAddress('bar', new Domain(['example', 'com']))),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('foo@example.com , bar@example.com'));
	}
	
	public function testParseMultipleMailboxes() : void
	{
		$expected = new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc'),
			new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc'),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('"Foo, Inc" <foo@example.com> , "Bar, Inc" <bar@example.com>'));
	}
	
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MailboxListParser();
	}
	
}
