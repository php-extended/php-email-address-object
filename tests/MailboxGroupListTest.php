<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\Email\EmailAddress;
use PhpExtended\Email\EmailAddressList;
use PhpExtended\Email\Mailbox;
use PhpExtended\Email\MailboxGroup;
use PhpExtended\Email\MailboxGroupInterface;
use PhpExtended\Email\MailboxGroupList;
use PhpExtended\Email\MailboxList;
use PHPUnit\Framework\TestCase;

/**
 * MailboxGroupListTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Email\MailboxGroupList
 *
 * @internal
 *
 * @small
 */
class MailboxGroupListTest extends TestCase
{
	
	/**
	 * MailboxGroupListTest class file.
	 * 
	 * @var MailboxGroupList
	 */
	protected MailboxGroupList $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('The Foobar Company: "Foo, Inc." <foo@example.com>, "Bar, Inc." <bar@example.com>; The Candy Company: "Caramel Gmbh." <caramel@example2.com>, "Nougat Gmbh." <nougat@example2.com>', $this->_object->__toString());
	}
	
	public function testGetCount() : void
	{
		$this->assertEquals(2, $this->_object->count());
	}
	
	public function testIteration() : void
	{
		$k = 0;
		
		foreach($this->_object as $i => $mailboxGroup)
		{
			$this->assertIsInt($i);
			$this->assertInstanceOf(MailboxGroupInterface::class, $mailboxGroup);
			$k++;
		}
		
		$this->assertEquals(2, $k);
	}
	
	public function testWithDomain() : void
	{
		$expected = $this->_object = new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
			]), 'The Foobar Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('caramel', new Domain(['example', 'com'])), 'Caramel Gmbh.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example', 'com'])), 'Nougat Gmbh.'),
			]), 'The Candy Company'),
		]);
		
		$this->assertEquals($expected, $this->_object->withDomain(new Domain(['example', 'com'])));
	}
	
	public function testFilterByDomain() : void
	{
		$expected = $this->_object = new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
			]), 'The Foobar Company'),
		]);
		
		$this->assertEquals($expected, $this->_object->filterByDomain(new Domain(['example', 'com'])));
	}
	
	public function testWithEmailAddress() : void
	{
		$expected = new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
			]), 'The Foobar Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('caramel', new Domain(['example2', 'com'])), 'Caramel Gmbh.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
			]), 'The Candy Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('train', new Domain(['example', 'com'])), 'Railroad, Inc.'),
			]), 'The Rail Company'),
		]);
		
		$this->assertEquals($expected, $this->_object->withEmailAddress(new EmailAddress('train', new Domain(['example', 'com'])), 'Railroad, Inc.', 'The Rail Company'));
	}
	
	public function testWithEmailAddressFound() : void
	{
		$expected = new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
			]), 'The Foobar Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('caramel', new Domain(['example2', 'com'])), 'Caramel Gmbh.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
				new Mailbox(new EmailAddress('oreo', new Domain(['example2', 'com'])), 'Oreo Gmbh.'),
			]), 'The Candy Company'),
		]);
		
		$this->assertEquals($expected, $this->_object->withEmailAddress(new EmailAddress('oreo', new Domain(['example2', 'com'])), 'Oreo Gmbh.', 'The Candy Company'));
	}
	
	public function testWithEmailAddressList() : void
	{
		$expected = new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
			]), 'The Foobar Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('caramel', new Domain(['example2', 'com'])), 'Caramel Gmbh.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
			]), 'The Candy Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('qux', new Domain(['example', 'com']))),
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com']))),
			])),
		]);
		
		$this->assertEquals($expected, $this->_object->withEmailAddressList(new EmailAddressList([
			new EmailAddress('qux', new Domain(['example', 'com'])),
			new EmailAddress('bar', new Domain(['example', 'com'])),
		])));
	}
	
	public function testWithEmailAddressListFound() : void
	{
		$expected = new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
				new Mailbox(new EmailAddress('qux', new Domain(['example', 'com']))),
			]), 'The Foobar Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('caramel', new Domain(['example2', 'com'])), 'Caramel Gmbh.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
			]), 'The Candy Company'),
		]);
		
		$this->assertEquals($expected, $this->_object->withEmailAddressList(new EmailAddressList([
			new EmailAddress('qux', new Domain(['example', 'com'])),
			new EmailAddress('bar', new Domain(['example', 'com'])),
		]), 'The Foobar Company'));
	}
	
	public function testWithMailbox() : void
	{
		$expected = new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
			]), 'The Foobar Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('caramel', new Domain(['example2', 'com'])), 'Caramel Gmbh.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
			]), 'The Candy Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('train', new Domain(['example', 'com'])), 'Railroad, Inc.'),
			]), 'The Rail Company'),
		]);
		
		$this->assertEquals($expected, $this->_object->withMailbox(new Mailbox(new EmailAddress('train', new Domain(['example', 'com'])), 'Railroad, Inc.'), 'The Rail Company'));
	}
	
	public function testWithMailboxFound() : void
	{
		$expected = new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
			]), 'The Foobar Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('caramel', new Domain(['example2', 'com'])), 'Caramel Gmbh.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
				new Mailbox(new EmailAddress('oreo', new Domain(['example2', 'com'])), 'Oreo Gmbh.'),
			]), 'The Candy Company'),
		]);
		
		$this->assertEquals($expected, $this->_object->withMailbox(new Mailbox(new EmailAddress('oreo', new Domain(['example2', 'com'])), 'Oreo Gmbh.'), 'The Candy Company'));
	}
	
	public function testWithMailboxList() : void
	{
		$expected = new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
			]), 'The Foobar Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('caramel', new Domain(['example2', 'com'])), 'Caramel Gmbh.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
			]), 'The Candy Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('train', new Domain(['example', 'com'])), 'Railroad, Inc.'),
				new Mailbox(new EmailAddress('truck', new Domain(['example', 'com'])), 'Truckload, Inc.'),
			]), 'The Railroad Company'),
		]);
		
		$this->assertEquals($expected, $this->_object->withMailboxList(new MailboxList([
			new Mailbox(new EmailAddress('train', new Domain(['example', 'com'])), 'Railroad, Inc.'),
			new Mailbox(new EmailAddress('truck', new Domain(['example', 'com'])), 'Truckload, Inc.'),
		]), 'The Railroad Company'));
	}
	
	public function testWithMailboxListFound() : void
	{
		$expected = new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
			]), 'The Foobar Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('caramel', new Domain(['example2', 'com'])), 'Caramel Gmbh.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
				new Mailbox(new EmailAddress('oreo', new Domain(['example2', 'com'])), 'Oreo Gmbh.'),
			]), 'The Candy Company'),
		]);
		
		$this->assertEquals($expected, $this->_object->withMailboxList(new MailboxList([
			new Mailbox(new EmailAddress('oreo', new Domain(['example2', 'com'])), 'Oreo Gmbh.'),
			new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
		]), 'The Candy Company'));
	}
	
	public function testWithMailboxGroup() : void
	{
		$expected = new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
			]), 'The Foobar Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('caramel', new Domain(['example2', 'com'])), 'Caramel Gmbh.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
			]), 'The Candy Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('train', new Domain(['example', 'com'])), 'Railroad, Inc.'),
				new Mailbox(new EmailAddress('truck', new Domain(['example', 'com'])), 'Truckload, Inc.'),
			]), 'The Railroad Company'),
		]);
		
		$this->assertEquals($expected, $this->_object->withMailboxGroup(new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('train', new Domain(['example', 'com'])), 'Railroad, Inc.'),
			new Mailbox(new EmailAddress('truck', new Domain(['example', 'com'])), 'Truckload, Inc.'),
		]), 'The Railroad Company')));
	}
	
	public function testWithMailboxGroupFound() : void
	{
		$expected = new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
			]), 'The Foobar Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('caramel', new Domain(['example2', 'com'])), 'Caramel Gmbh.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
				new Mailbox(new EmailAddress('oreo', new Domain(['example2', 'com'])), 'Oreo Gmbh.'),
			]), 'The Candy Company'),
		]);
		
		$this->assertEquals($expected, $this->_object->withMailboxGroup(new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('oreo', new Domain(['example2', 'com'])), 'Oreo Gmbh.'),
			new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
		]), 'The Candy Company')));
	}
	
	public function testWithMailboxGroupList() : void
	{
		$expected = new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
			]), 'The Foobar Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('caramel', new Domain(['example2', 'com'])), 'Caramel Gmbh.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
				new Mailbox(new EmailAddress('oreo', new Domain(['example2', 'com'])), 'Oreo Gmbh.'),
			]), 'The Candy Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('train', new Domain(['example', 'com'])), 'Railroad, Inc.'),
				new Mailbox(new EmailAddress('truck', new Domain(['example', 'com'])), 'Truckload, Inc.'),
			]), 'The Railroad Company'),
		]);
		
		$this->assertEquals($expected, $this->_object->withMailboxGroupList(new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('train', new Domain(['example', 'com'])), 'Railroad, Inc.'),
				new Mailbox(new EmailAddress('truck', new Domain(['example', 'com'])), 'Truckload, Inc.'),
			]), 'The Railroad Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('oreo', new Domain(['example2', 'com'])), 'Oreo Gmbh.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
			]), 'The Candy Company'),
		])));
	}
	
	public function testGetCanonicalRepresentation() : void
	{
		$this->assertEquals('The Foobar Company: "Foo, Inc." <foo@example.com>, "Bar, Inc." <bar@example.com>; The Candy Company: "Caramel Gmbh." <caramel@example2.com>, "Nougat Gmbh." <nougat@example2.com>', $this->_object->getCanonicalRepresentation());
	}
	
	public function testIsEmpty() : void
	{
		$this->assertFalse($this->_object->isEmpty());
	}
	
	public function testCollectDisplayNames() : void
	{
		$expected = new ArrayIterator(['Foo, Inc.', 'Bar, Inc.', 'Caramel Gmbh.', 'Nougat Gmbh.']);
		
		$this->assertEquals($expected, $this->_object->collectDisplayNames());
	}
	
	public function testCollectDomains() : void
	{
		$expected = new ArrayIterator([
			new Domain(['example', 'com']),
			new Domain(['example2', 'com']),
		]);
		
		$this->assertEquals($expected, $this->_object->collectDomains());
	}
	
	public function testCollectEmailAddresses() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
			new EmailAddress('bar', new Domain(['example', 'com'])),
			new EmailAddress('caramel', new Domain(['example2', 'com'])),
			new EmailAddress('nougat', new Domain(['example2', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->collectEmailAddresses());
	}
	
	public function testCollectMailboxes() : void
	{
		$expected = new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
			new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
			new Mailbox(new EmailAddress('caramel', new Domain(['example2', 'com'])), 'Caramel Gmbh.'),
			new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
		]);
		
		$this->assertEquals($expected, $this->_object->collectMailboxes());
	}
	
	public function testEqualsVoid() : void
	{
		$this->assertFalse($this->_object->equals(null));
	}
	
	public function testEqualsNotSame() : void
	{
		$this->assertFalse($this->_object->equals(new MailboxGroupList([])));
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals($this->_object));
	}
	
	public function testContainsEmailAddress() : void
	{
		$this->assertTrue($this->_object->containsEmailAddress(new EmailAddress('bar', new Domain(['example', 'com']))));
	}
	
	public function testDoesNotContainEmailAddress() : void
	{
		$this->assertFalse($this->_object->containsEmailAddress(new EmailAddress('qux', new Domain(['example', 'com']))));
	}
	
	public function testContainsEmailAddressList() : void
	{
		$this->assertTrue($this->_object->containsEmailAddressList(new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
			new EmailAddress('bar', new Domain(['example', 'com'])),
		])));
	}
	
	public function testContainsEmailAddressList2() : void
	{
		$this->assertTrue($this->_object->containsEmailAddressList(new EmailAddressList([
			new EmailAddress('bar', new Domain(['example', 'com'])),
			new EmailAddress('caramel', new Domain(['example2', 'com'])),
		])));
	}
	
	public function testDoesNotContainEmailAddressList() : void
	{
		$this->assertFalse($this->_object->containsEmailAddressList(new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
			new EmailAddress('qux', new Domain(['example', 'com'])),
		])));
	}
	
	public function testContainsMailbox() : void
	{
		$this->assertTrue($this->_object->containsMailbox(new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.')));
	}
	
	public function testDoesNotContainMailbox() : void
	{
		$this->assertFalse($this->_object->containsMailbox(new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'FOOBAR')));
	}
	
	public function testContainsMailboxIgnoreLabels() : void
	{
		$this->assertTrue($this->_object->containsMailboxIgnoreLabels(new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'FOOBAR')));
	}
	
	public function testDoesNotContainMailboxIgnoreLabels() : void
	{
		$this->assertFalse($this->_object->containsMailboxIgnoreLabels(new Mailbox(new EmailAddress('qux', new Domain(['example', 'com'])), 'Qux, Inc.')));
	}
	
	public function testContainsMailboxList() : void
	{
		$this->assertTrue($this->_object->containsMailboxList(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
			new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
		])));
	}
	
	public function testDoesNotContainMailboxList() : void
	{
		$this->assertFalse($this->_object->containsMailboxList(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Bar, Inc.'),
			new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Foo, Inc.'),
		])));
	}
	
	public function testContainsMailboxListIgnoreLabels() : void
	{
		$this->assertTrue($this->_object->containsMailboxListIgnoreLabels(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Bar, Inc.'),
			new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Foo, Inc.'),
		])));
	}
	
	public function testDoesNotContainMailboxListIgnoreLabels() : void
	{
		$this->assertFalse($this->_object->containsMailboxListIgnoreLabels(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
			new Mailbox(new EmailAddress('qux', new Domain(['example', 'com'])), 'Qux, Inc.'),
		])));
	}
	
	public function testContainsMailboxGroup() : void
	{
		$this->assertTrue($this->_object->containsMailboxGroup(new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
		]), 'The Foobar Company')));
	}
	
	public function testDoesNotContainMailboxGroup() : void
	{
		$this->assertFalse($this->_object->containsMailboxGroup(new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
		])), 'The Santa Company'));
	}
	
	public function testContainsMailboxGroupIgnoreLabels() : void
	{
		$this->assertTrue($this->_object->containsMailboxGroupIgnoreLabels(new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Foo, Inc.'),
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Bar, Inc.'),
		])), 'The Santa Company'));
	}
	
	public function testDoesNotContainMailboxGroupIgnoreLabels() : void
	{
		$this->assertFalse($this->_object->containsMailboxGroupIgnoreLabels(new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Bar, Inc.'),
			new Mailbox(new EmailAddress('qux', new Domain(['example', 'com'])), 'Foo, Inc.'),
		])), 'The Santa Company'));
	}
	
	public function testContainsMailboxGroupList() : void
	{
		$this->assertTrue($this->_object->containsMailboxGroupList(new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('caramel', new Domain(['example2', 'com'])), 'Caramel Gmbh.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
			]), 'The Candy Company'),
		])));
	}
	
	public function testDoesNotContainMailboxGroupList() : void
	{
		$this->assertFalse($this->_object->containsMailboxGroupList(new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
			]), 'The Candy Company'),
		])));
	}
	
	public function testContainsMailboxGroupListIgnoreLabels() : void
	{
		$this->assertTrue($this->_object->containsMailboxGroupListIgnoreLabels(new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
			]), 'The Candy Company'),
		])));
	}
	
	public function testDoesNotContainMailboxGroupListIgnoreLabels() : void
	{
		$this->assertFalse($this->_object->containsMailboxGroupListIgnoreLabels(new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('oreo', new Domain(['example', 'com'])), 'Oreo, Inc.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
			]), 'The Candy Company'),
		])));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MailboxGroupList([
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
				new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
			]), 'The Foobar Company'),
			new MailboxGroup(new MailboxList([
				new Mailbox(new EmailAddress('caramel', new Domain(['example2', 'com'])), 'Caramel Gmbh.'),
				new Mailbox(new EmailAddress('nougat', new Domain(['example2', 'com'])), 'Nougat Gmbh.'),
			]), 'The Candy Company'),
		]);
	}
	
}
