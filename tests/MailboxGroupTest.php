<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\Email\EmailAddress;
use PhpExtended\Email\EmailAddressList;
use PhpExtended\Email\Mailbox;
use PhpExtended\Email\MailboxGroup;
use PhpExtended\Email\MailboxInterface;
use PhpExtended\Email\MailboxList;
use PHPUnit\Framework\TestCase;

/**
 * MailboxGroupTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Email\MailboxGroup
 *
 * @internal
 *
 * @small
 */
class MailboxGroupTest extends TestCase
{
	
	/**
	 * The mailbox group to test.
	 * 
	 * @var MailboxGroup
	 */
	protected MailboxGroup $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('"The Foobar (TM) Company": "Foo, Inc." <foo@example.com>, "Bar, Inc." <bar@example2.com>', $this->_object->__toString());
	}
	
	public function testGetDisplayName() : void
	{
		$this->assertEquals('The Foobar (TM) Company', $this->_object->getDisplayName());
	}
	
	public function testCount() : void
	{
		$this->assertEquals(2, $this->_object->count());
	}
	
	public function testIteration() : void
	{
		$k = 0;
		
		foreach($this->_object as $i => $mailbox)
		{
			$this->assertIsInt($i);
			$this->assertInstanceOf(MailboxInterface::class, $mailbox);
			$k++;
		}
		
		$this->assertEquals(2, $k);
	}
	
	public function testWithDomain() : void
	{
		$expected = new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
			new Mailbox(new EmailAddress('bar', new Domain(['example', 'com'])), 'Bar, Inc.'),
		]), 'The Foobar (TM) Company');
		
		$this->assertEquals($expected, $this->_object->withDomain(new Domain(['example', 'com'])));
	}
	
	public function testFilterByDomain() : void
	{
		$expected = new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
		]), 'The Foobar (TM) Company');
		
		$this->assertEquals($expected, $this->_object->filterByDomain(new Domain(['example', 'com'])));
	}
	
	public function testWithDisplayName() : void
	{
		$this->assertEquals('The Santa Company: "Foo, Inc." <foo@example.com>, "Bar, Inc." <bar@example2.com>', $this->_object->withDisplayName('The Santa Company')->__toString());
	}
	
	public function testWithEmailAddress() : void
	{
		$expected = new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
			new Mailbox(new EmailAddress('bar', new Domain(['example2', 'com'])), 'Bar, Inc.'),
			new Mailbox(new EmailAddress('qux', new Domain(['example', 'com'])), 'Qux, Inc.'),
		]), 'The Foobar (TM) Company');
		
		$this->assertEquals($expected, $this->_object->withEmailAddress(new EmailAddress('qux', new Domain(['example', 'com'])), 'Qux, Inc.'));
	}
	
	public function testWithEmailAddressList() : void
	{
		$expected = new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
			new Mailbox(new EmailAddress('bar', new Domain(['example2', 'com'])), 'Bar, Inc.'),
			new Mailbox(new EmailAddress('qux', new Domain(['example', 'com']))),
		]), 'The Foobar (TM) Company');
		
		$this->assertEquals($expected, $this->_object->withEmailAddressList(new EmailAddressList([
			new EmailAddress('qux', new Domain(['example', 'com'])),
			new EmailAddress('bar', new Domain(['example2', 'com'])),
		])));
	}
	
	public function testWithMailbox() : void
	{
		$expected = new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
			new Mailbox(new EmailAddress('bar', new Domain(['example2', 'com'])), 'Bar, Inc.'),
			new Mailbox(new EmailAddress('qux', new Domain(['example', 'com'])), 'Qux, Inc.'),
		]), 'The Foobar (TM) Company');
		
		$this->assertEquals($expected, $this->_object->withMailbox(new Mailbox(new EmailAddress('qux', new Domain(['example', 'com'])), 'Qux, Inc.')));
	}
	
	public function testWithMailboxList() : void
	{
		$expected = new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
			new Mailbox(new EmailAddress('bar', new Domain(['example2', 'com'])), 'Bar, Inc.'),
			new Mailbox(new EmailAddress('qux', new Domain(['example', 'com'])), 'Qux, Inc.'),
		]), 'The Foobar (TM) Company');
		
		$this->assertEquals($expected, $this->_object->withMailboxList(new MailboxList([
			new Mailbox(new EmailAddress('qux', new Domain(['example', 'com'])), 'Qux, Inc.'),
			new Mailbox(new EmailAddress('bar', new Domain(['example2', 'com'])), 'FOOBAR!!!'),
		])));
	}
	
	public function testCollectDisplayNames() : void
	{
		$expected = new ArrayIterator(['Foo, Inc.', 'Bar, Inc.']);
		
		$this->assertEquals($expected, $this->_object->collectDisplayNames());
	}
	
	public function testIsEmpty() : void
	{
		$this->assertFalse($this->_object->isEmpty());
	}
	
	public function testCollectEmailAddresses() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
			new EmailAddress('bar', new Domain(['example2', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->collectEmailAddresses());
	}
	
	public function testGetCanonicalRepresentation() : void
	{
		$this->assertEquals('"The Foobar (TM) Company": "Foo, Inc." <foo@example.com>, "Bar, Inc." <bar@example2.com>', $this->_object->getCanonicalRepresentation());
	}
	
	public function testGetCanonicalRepresentationNoDisplayName() : void
	{
		$this->assertEquals('"Foo, Inc." <foo@example.com>, "Bar, Inc." <bar@example2.com>', (new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
			new Mailbox(new EmailAddress('bar', new Domain(['example2', 'com'])), 'Bar, Inc.'),
		])))->getCanonicalRepresentation());
	}
	
	public function testCollectDomains() : void
	{
		$expected = new ArrayIterator([
			new Domain(['example', 'com']),
			new Domain(['example2', 'com']),
		]);
		
		$this->assertEquals($expected, $this->_object->collectDomains());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals($this->_object));
	}
	
	public function testContainsEmailAddress() : void
	{
		$this->assertTrue($this->_object->containsEmailAddress(new EmailAddress('bar', new Domain(['example2', 'com']))));
	}
	
	public function testDoesNotContainEmailAddress() : void
	{
		$this->assertFalse($this->_object->containsEmailAddress(new EmailAddress('qux', new Domain(['example', 'com']))));
	}
	
	public function testContainsEmailAddressList() : void
	{
		$this->assertTrue($this->_object->containsEmailAddressList(new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
			new EmailAddress('bar', new Domain(['example2', 'com'])),
		])));
	}
	
	public function testDoesNotContainEmailAddressList() : void
	{
		$this->assertFalse($this->_object->containsEmailAddressList(new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
			new EmailAddress('qux', new Domain(['example', 'com'])),
		])));
	}
	
	public function testContainsMailbox() : void
	{
		$this->assertTrue($this->_object->containsMailbox(new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.')));
	}
	
	public function testDoesNotContainMailbox() : void
	{
		$this->assertFalse($this->_object->containsMailbox(new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'FOOBAR')));
	}
	
	public function testContainsMailboxIgnoreLabels() : void
	{
		$this->assertTrue($this->_object->containsMailboxIgnoreLabels(new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'FOOBAR')));
	}
	
	public function testDoesNotContainMailboxIgnoreLabels() : void
	{
		$this->assertFalse($this->_object->containsMailboxIgnoreLabels(new Mailbox(new EmailAddress('qux', new Domain(['example', 'com'])), 'Qux, Inc.')));
	}
	
	public function testContainsMailboxList() : void
	{
		$this->assertTrue($this->_object->containsMailboxList(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
			new Mailbox(new EmailAddress('bar', new Domain(['example2', 'com'])), 'Bar, Inc.'),
		])));
	}
	
	public function testDoesNotContainMailboxList() : void
	{
		$this->assertFalse($this->_object->containsMailboxList(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Bar, Inc.'),
			new Mailbox(new EmailAddress('bar', new Domain(['example2', 'com'])), 'Foo, Inc.'),
		])));
	}
	
	public function testContainsMailboxListIgnoreLabels() : void
	{
		$this->assertTrue($this->_object->containsMailboxListIgnoreLabels(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Bar, Inc.'),
			new Mailbox(new EmailAddress('bar', new Domain(['example2', 'com'])), 'Foo, Inc.'),
		])));
	}
	
	public function testDoesNotContainMailboxListIgnoreLabels() : void
	{
		$this->assertFalse($this->_object->containsMailboxListIgnoreLabels(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
			new Mailbox(new EmailAddress('qux', new Domain(['example', 'com'])), 'Qux, Inc.'),
		])));
	}
	
	public function testContainsMailboxGroup() : void
	{
		$this->assertTrue($this->_object->containsMailboxGroup(new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('bar', new Domain(['example2', 'com'])), 'Bar, Inc.'),
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
		]), 'The Foobar (TM) Company')));
	}
	
	public function testDoesNotContainMailboxGroup() : void
	{
		$this->assertFalse($this->_object->containsMailboxGroup(new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('bar', new Domain(['example2', 'com'])), 'Bar, Inc.'),
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
		])), 'The Santa Company'));
	}
	
	public function testContainsMailboxGroupIgnoreLabels() : void
	{
		$this->assertTrue($this->_object->containsMailboxGroupIgnoreLabels(new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('bar', new Domain(['example2', 'com'])), 'Foo, Inc.'),
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Bar, Inc.'),
		])), 'The Santa Company'));
	}
	
	public function testDoesNotContainMailboxGroupIgnoreLabels() : void
	{
		$this->assertFalse($this->_object->containsMailboxGroupIgnoreLabels(new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Bar, Inc.'),
			new Mailbox(new EmailAddress('qux', new Domain(['example', 'com'])), 'Foo, Inc.'),
		])), 'The Santa Company'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MailboxGroup(new MailboxList([
			new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc.'),
			new Mailbox(new EmailAddress('bar', new Domain(['example2', 'com'])), 'Bar, Inc.'),
		]), 'The Foobar (TM) Company');
	}
	
}
