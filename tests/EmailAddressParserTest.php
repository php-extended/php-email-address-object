<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\Email\EmailAddress;
use PhpExtended\Email\EmailAddressParser;
use PhpExtended\Ip\Ipv4Address;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * EmailAddressParser class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Email\EmailAddressParser
 *
 * @internal
 *
 * @small
 */
class EmailAddressParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var EmailAddressParser
	 */
	protected EmailAddressParser $_object;
	
	public function testParseEmptyString() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse(null);
	}
	
	public function testParseEmailAddressNoLocalPart() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('@example.com');
	}
	
	public function testParseEmailAddressNoDomain() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('local.part@');
	}
	
	public function testParseEmailAddressNoMeaningfulDomain() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('local.part@.');
	}
	
	// https://gist.github.com/cjaoude/fd9910626629b53c4d25 below
	
	public function testParseEmailAddress001() : void
	{
		$this->assertEquals(new EmailAddress('email', new Domain(['example', 'com'])), $this->_object->parse('email@example.com'));
	}
	
	public function testParseEmailAddress002() : void
	{
		$this->assertEquals(new EmailAddress('firstname.lastname', new Domain(['example', 'com'])), $this->_object->parse('firstname.lastname@example.com'));
	}
	
	public function testParseEmailAddress003() : void
	{
		$this->assertEquals(new EmailAddress('email', new Domain(['subdomain', 'example', 'com'])), $this->_object->parse('email@subdomain.example.com'));
	}
	
	public function testParseEmailAddress004() : void
	{
		$this->assertEquals(new EmailAddress('firstname+lastname', new Domain(['example', 'com'])), $this->_object->parse('firstname+lastname@example.com'));
	}
	
	public function testParseEmailAddress005() : void
	{
		// domain, not ip address as [ are not used
		$this->assertEquals(new EmailAddress('email', new Domain(['123', '123', '123', '123'])), $this->_object->parse('email@123.123.123.123'));
	}
	
	public function testParseEmailAddress006() : void
	{
		$this->assertEquals(new EmailAddress('email', null, new Ipv4Address(123, 123, 123, 123)), $this->_object->parse('email@[123.123.123.123]'));
	}
	
	public function testParseEmailAddress007() : void
	{
		$this->assertEquals(new EmailAddress('email', new Domain(['example', 'com'])), $this->_object->parse('"email"@example.com'));
	}
	
	public function testParseEmailAddress008() : void
	{
		$this->assertEquals(new EmailAddress('1234567890', new Domain(['example', 'com'])), $this->_object->parse('1234567890@example.com'));
	}
	
	public function testParseEmailAddress009() : void
	{
		$this->assertEquals(new EmailAddress('email', new Domain(['example-one', 'com'])), $this->_object->parse('email@example-one.com'));
	}
	
	public function testParseEmailAddress010() : void
	{
		$this->assertEquals(new EmailAddress('_______', new Domain(['example', 'com'])), $this->_object->parse('_______@example.com'));
	}
	
	public function testParseEmailAddress011() : void
	{
		$this->assertEquals(new EmailAddress('email', new Domain(['example', 'name'])), $this->_object->parse('email@example.name'));
	}
	
	public function testParseEmailAddress012() : void
	{
		$this->assertEquals(new EmailAddress('email', new Domain(['example', 'museum'])), $this->_object->parse('email@example.museum'));
	}
	
	public function testParseEmailAddress013() : void
	{
		$this->assertEquals(new EmailAddress('email', new Domain(['example', 'co', 'jp'])), $this->_object->parse('email@example.co.jp'));
	}
	
	public function testParseEmailAddress014() : void
	{
		$this->assertEquals(new EmailAddress('firstname-lastname', new Domain(['example', 'com'])), $this->_object->parse('firstname-lastname@example.com'));
	}
	
	public function testParseEmailAddress015() : void
	{
		$this->assertEquals(new EmailAddress('much.more unusual', new Domain(['example', 'com'])), $this->_object->parse('much."more\\ unusual"@example.com'));
	}
	
	public function testParseEmailAddress016() : void
	{
		$this->assertEquals(new EmailAddress('very.unusual.@.unusual.com', new Domain(['example', 'com'])), $this->_object->parse('very.unusual."@".unusual.com@example.com'));
	}
	
	public function testParseEmailAddress017() : void
	{
		$this->assertEquals(new EmailAddress('very.(),:;<>[].VERY.very@\\\\ \\"very.unusual', new Domain(['strange', 'example', 'com'])), $this->_object->parse('very."(),:;<>[]".VERY."very@\\\\\\ \\"very".unusual@strange.example.com'));
	}
	
	public function testParseEmailAddress018() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('#@%^%#$@#$@#.com');
	}
	
	public function testParseEmailAddress019() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('@example.com');
	}
	
	public function testParseEmailAddress020() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('Joe Smith <email@example.com>');
	}
	
	public function testParseEmailAddress022() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('email.example.com');
	}
	
	public function testParseEmailAddress023() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('email@example@example.com');
	}
	
	public function testParseEmailAddress024() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('.email@example.com');
	}
	
	public function testParseEmailAddress025() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('email.@example.com');
	}
	
	public function testParseEmailAddress026() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('email..email@example.com');
	}
	
	public function testParseEmailAddress027() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('あいうえお@example.com');
	}
	
	public function testParseEmailAddress028() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('email@example.com (Joe Smith)');
	}
	
	public function testParseEmailAddress029() : void
	{
		$this->assertEquals('email@example', $this->_object->parse('email@example')->__toString());
	}
	
	public function testParseEmailAddress030() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('email@-example.com');
	}
	
	public function testParseEmailAddress031() : void
	{
		$this->assertEquals('email@111.222.333.44444', $this->_object->parse('email@111.222.333.44444')->__toString());
	}
	
	public function testParseEmailAddress032() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('email@example..com');
	}
	
	public function testParseEmailAddress033() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('Abc..123@example.com');
	}
	
	public function testParseEmailAddress034() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('"(),:;<>[\\]@example.com');
	}
	
	public function testParseEmailAddress035() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('just"not"right@example.com');
	}
	
	public function testParseEmailAddress036() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('this\\ is"really"not\\allowed@example.com');
	}
	
	public function testParseEmailAddress037() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('email@[123.123.123.512]');
	}
	
	// https://github.com/dominicsayers/isemail/blob/master/test/tests.xml below
	
	public function testParseEmailAddress038() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test');
	}
	
	public function testParseEmailAddress039() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('@');
	}
	
	public function testParseEmailAddress040() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@');
	}
	
	public function testParseEmailAddress041() : void
	{
		$this->assertEquals('test@io', $this->_object->parse('test@io')->__toString());
	}
	
	public function testParseEmailAddress042() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('@io');
	}
	
	public function testParseEmailAddress043() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('@iana.org');
	}
	
	public function testParseEmailAddress044() : void
	{
		$this->assertEquals('test@iana.org', $this->_object->parse('test@iana.org')->__toString());
	}
	
	public function testParseEmailAddress045() : void
	{
		$this->assertEquals('test@nominet.org.uk', $this->_object->parse('test@nominet.org.uk')->__toString());
	}
	
	public function testParseEmailAddress046() : void
	{
		$this->assertEquals('test@about.museum', $this->_object->parse('test@about.museum')->__toString());
	}
	
	public function testParseEmailAddress047() : void
	{
		$this->assertEquals('a@iana.org', $this->_object->parse('a@iana.org')->__toString());
	}
	
	public function testParseEmailAddress048() : void
	{
		$this->assertEquals('test@e.com', $this->_object->parse('test@e.com')->__toString());
	}
	
	public function testParseEmailAddress049() : void
	{
		$this->assertEquals('test@iana.a', $this->_object->parse('test@iana.a')->__toString());
	}
	
	public function testParseEmailAddress050() : void
	{
		$this->assertEquals('test.test@iana.org', $this->_object->parse('test.test@iana.org')->__toString());
	}
	
	public function testParseEmailAddress051() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('.test@iana.org');
	}
	
	public function testParseEmailAddress052() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test.@iana.org');
	}
	
	public function testParseEmailAddress053() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test..iana.org');
	}
	
	public function testParseEmailAddress054() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test_exa-mple.com');
	}
	
	public function testParseEmailAddress055() : void
	{
		$this->assertEquals('!#$%&`*+/=?^`{|}~@iana.org', $this->_object->parse('!#$%&`*+/=?^`{|}~@iana.org')->__toString());
	}
	
	public function testParseEmailAddress056() : void
	{
		$this->assertEquals('"test@test"@iana.org', $this->_object->parse('test\\@test@iana.org')->__toString());
	}
	
	public function testParseEmailAddress057() : void
	{
		$this->assertEquals('123@iana.org', $this->_object->parse('123@iana.org')->__toString());
	}
	
	public function testParseEmailAddress058() : void
	{
		$this->assertEquals('test@123.com', $this->_object->parse('test@123.com')->__toString());
	}
	
	public function testParseEmailAddress059() : void
	{
		$this->assertEquals('test@iana.123', $this->_object->parse('test@iana.123')->__toString());
	}
	
	public function testParseEmailAddress060() : void
	{
		$this->assertEquals('test@255.255.255.255', $this->_object->parse('test@255.255.255.255')->__toString());
	}
	
	public function testParseEmailAddress061() : void
	{
		$this->assertEquals('abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghiklm@iana.org', $this->_object->parse('abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghiklm@iana.org')->__toString());
	}
	
	public function testParseEmailAddress062() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghiklmn@iana.org');
	}
	
	public function testParseEmailAddress063() : void
	{
		$this->assertEquals('test@abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl.com', $this->_object->parse('test@abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl.com')->__toString());
	}
	
	public function testParseEmailAddress064() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghiklm.com');
	}
	
	public function testParseEmailAddress065() : void
	{
		$this->assertEquals('test@mason-dixon.com', $this->_object->parse('test@mason-dixon.com')->__toString());
	}
	
	public function testParseEmailAddress066() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@-iana.org');
	}
	
	public function testParseEmailAddress067() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@iana-.com');
	}
	
	public function testParseEmailAddress068() : void
	{
		$this->assertEquals('test@c--n.com', $this->_object->parse('test@c--n.com')->__toString());
	}
	
	public function testParseEmailAddress069() : void
	{
		$this->assertEquals('test@iana.co-uk', $this->_object->parse('test@iana.co-uk')->__toString());
	}
	
	public function testParseEmailAddress070() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@.iana.org');
	}
	
	public function testParseEmailAddress071() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@iana.org.');
	}
	
	public function testParseEmailAddress072() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@iana..com');
	}
	
	public function testParseEmailAddress073() : void
	{
		$this->assertEquals('a@a.b.c.d.e.f.g.h.i.j.k.l.m.n.o.p.q.r.s.t.u.v.w.x.y.z.a.b.c.d.e.f.g.h.i.j.k.l.m.n.o.p.q.r.s.t.u.v.w.x.y.z.a.b.c.d.e.f.g.h.i.j.k.l.m.n.o.p.q.r.s.t.u.v.w.x.y.z.a.b.c.d.e.f.g.h.i.j.k.l.m.n.o.p.q.r.s.t.u.v.w.x.y.z.a.b.c.d.e.f.g.h.i.j.k.l.m.n.o.p.q.r.s.t.u.v', $this->_object->parse('a@a.b.c.d.e.f.g.h.i.j.k.l.m.n.o.p.q.r.s.t.u.v.w.x.y.z.a.b.c.d.e.f.g.h.i.j.k.l.m.n.o.p.q.r.s.t.u.v.w.x.y.z.a.b.c.d.e.f.g.h.i.j.k.l.m.n.o.p.q.r.s.t.u.v.w.x.y.z.a.b.c.d.e.f.g.h.i.j.k.l.m.n.o.p.q.r.s.t.u.v.w.x.y.z.a.b.c.d.e.f.g.h.i.j.k.l.m.n.o.p.q.r.s.t.u.v')->__toString());
	}
	
	public function testParseEmailAddress074() : void
	{
		$this->assertEquals('abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghiklm@abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghi', $this->_object->parse('abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghiklm@abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghi')->__toString());
	}
	
	public function testParseEmailAddress075() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghiklm@abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl');
	}
	
	public function testParseEmailAddress076() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('a@abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefg.hijk');
	}
	
	public function testParseEmailAddress077() : void
	{
		$this->assertEquals('test@iana.org', $this->_object->parse('"test"@iana.org')->__toString());
	}
	
	public function testParseEmailAddress078() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('""@iana.org');
	}
	
	public function testParseEmailAddress079() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('"""@iana.org');
	}
	
	public function testParseEmailAddress080() : void
	{
		$this->assertEquals('a@iana.org', $this->_object->parse('"\\a"@iana.org')->__toString());
	}
	
	public function testParseEmailAddress081() : void
	{
		$this->assertEquals('"\\""@iana.org', $this->_object->parse('"\\""@iana.org')->__toString());
	}
	
	public function testParseEmailAddress082() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('"\\"@iana.org');
	}
	
	public function testParseEmailAddress083() : void
	{
		$this->assertEquals('"\\\\"@iana.org', $this->_object->parse('"\\\\"@iana.org')->__toString());
	}
	
	public function testParseEmailAddress084() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test"@iana.org');
	}
	
	public function testParseEmailAddress085() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('"test@iana.org');
	}
	
	public function testParseEmailAddress086() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('"test"test@iana.org');
	}
	
	public function testParseEmailAddress087() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test"text"@iana.org');
	}
	
	public function testParseEmailAddress088() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('"test""test"@iana.org');
	}
	
	public function testParseEmailAddress089() : void
	{
		$this->assertEquals('test.test@iana.org', $this->_object->parse('"test"."test"@iana.org')->__toString());
	}
	
	public function testParseEmailAddress090() : void
	{
		$this->assertEquals('"test test"@iana.org', $this->_object->parse('"test\\ test"@iana.org')->__toString());
	}
	
	public function testParseEmailAddress091() : void
	{
		$this->assertEquals('test.test@iana.org', $this->_object->parse('"test".test@iana.org')->__toString());
	}
	
	public function testParseEmailAddress092() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("\"test\x00\"@iana.org");
	}
	
	public function testParseEmailAddress093() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("\"test\\\x00\"@iana.org");
	}
	
	public function testParseEmailAddress094() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('"abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghj"@iana.org');
	}
	
	public function testParseEmailAddress095() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('"abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefg\\hi"@iana.org');
	}
	
	public function testParseEmailAddress096() : void
	{
		$this->assertEquals('test@[255.255.255.255]', $this->_object->parse('test@[255.255.255.255]')->__toString());
	}
	
	public function testParseEmailAddress097() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@a[255.255.255.255]');
	}
	
	public function testParseEmailAddress098() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[255.255.255]');
	}
	
	public function testParseEmailAddress099() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[255.255.255.255.255]');
	}
	
	public function testParseEmailAddress100() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[255.255.255.256]');
	}
	
// 	public function testParseEmailAddress101() : void
// 	{
// 		$this->assertEquals('test@[IPv6:1111:2222:3333:4444:5555:6666:7777:8888]', $this->_object->parse('test@[1111:2222:3333:4444:5555:6666:7777:8888]')->__toString());
// 	}
	
	public function testParseEmailAddress102() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[IPv6:1111:2222:3333:4444:5555:6666:7777]');
	}
	
// 	public function testParseEmailAddress103() : void
// 	{
// 		$this->assertEquals('test@[IPv6:1111:2222:3333:4444:5555:6666:7777:8888]', $this->_object->parse('test@[IPv6:1111:2222:3333:4444:5555:6666:7777:8888]')->__toString());
// 	}
	
	public function testParseEmailAddress104() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[IPv6:1111:2222:3333:4444:5555:6666:7777:8888:9999]');
	}
	
	public function testParseEmailAddress105() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[IPv6:1111:2222:3333:4444:5555:6666:7777:888G]');
	}
	
// 	public function testParseEmailAddress106() : void
// 	{
// 		$this->assertEquals('test@[IPv6:1111:2222:3333:4444:5555:6666::8888]', $this->_object->parse('test@[IPv6:1111:2222:3333:4444:5555:6666::8888]')->__toString());
// 	}
	
// 	public function testParseEmailAddress107() : void
// 	{
// 		$this->assertEquals('test@[IPv6:1111:2222:3333:4444:5555::8888]', $this->_object->parse('test@[IPv6:1111:2222:3333:4444:5555::8888]')->__toString());
// 	}
	
// 	public function testParseEmailAddress108() : void
// 	{
// 		$this->assertEquals('test@[IPv6:1111:2222:3333:4444:5555:6666:7777:8888]', $this->_object->parse('test@[IPv6:1111:2222:3333:4444:5555:6666::7777:8888]')->__toString());
// 	}
	
	public function testParseEmailAddress109() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[IPv6::3333:4444:5555:6666:7777:8888]');
	}
	
// 	public function testParseEmailAddress110() : void
// 	{
// 		$this->assertEquals('test@[IPv6:::3333:4444:5555:6666:7777:8888]', $this->_object->parse('test@[IPv6:::3333:4444:5555:6666:7777:8888]')->__toString());
// 	}
	
	public function testParseEmailAddress111() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[IPv6:1111::4444:5555::8888]');
	}
	
// 	public function testParseEmailAddress112() : void
// 	{
// 		$this->assertEquals('test@[IPv6:::]', $this->_object->parse('test@[IPv6:::]'));
// 	}
	
	public function testParseEmailAddress113() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[IPv6:1111:2222:3333:4444:5555:255.255.255.255]');
	}
	
// 	public function testParseEmailAddress114() : void
// 	{
// 		$this->assertEquals('test@[IPv6:1111:2222:3333:4444:5555:6666:FFFF:FFFF]', $this->_object->parse('test@[IPv6:1111:2222:3333:4444:5555:6666:255.255.255.255]')->__toString());
// 	}
	
	public function testParseEmailAddress115() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[IPv6:1111:2222:3333:4444:5555:6666:7777:255.255.255.255]');
	}
	
// 	public function testParseEmailAddress116() : void
// 	{
// 		$this->assertEquals('test@[IPv6:1111:2222:3333:4444::FFFF:FFFF]', $this->_object->parse('test@[IPv6:1111:2222:3333:4444::255.255.255.255]')->__toString());
// 	}
	
// 	public function testParseEmailAddress117() : void
// 	{
// 		$this->assertEquals('test@[IPv6:1111:2222:3333:4444:5555:6666:FFFF:FFFF]', $this->_object->parse('test@[IPv6:1111:2222:3333:4444:5555:6666::255.255.255.255]')->__toString());
// 	}
	
	public function testParseEmailAddress118() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[IPv6:1111:2222:3333:4444:::255.255.255.255]');
	}
	
	public function testParseEmailAddress119() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[IPv6::255.255.255.255]');
	}
	
	public function testParseEmailAddress120() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse(' test @iana.org');
	}
	
	public function testParseEmailAddress121() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@ iana .com');
	}
	
	public function testParseEmailAddress122() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test . test@iana.org');
	}
	
	public function testParseEmailAddress123() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("\x0D\x0A test@iana.org");
	}
	
	public function testParseEmailAddress124() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("\x0D\x0A \x0D\x0A test@iana.org");
	}
	
// 	public function testParseEmailAddress125() : void
// 	{
// 		$this->assertEquals('(comment)test@iana.org', $this->_object->parse('(comment)test@iana.org')->__toString());
// 	}
	
	public function testParseEmailAddress126() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('((comment)test@iana.org');
	}
	
// 	public function testParseEmailAddress127() : void
// 	{
// 		$this->assertEquals('(comment(comment))test@iana.org', $this->_object->parse('(comment(comment))test@iana.org')->__toString());
// 	}
	
// 	public function testParseEmailAddress128() : void
// 	{
// 		$this->assertEquals('test@(comment)iana.org', $this->_object->parse('test@(comment)iana.org')->__toString());
// 	}
	
// 	public function testParseEmailAddress129() : void
// 	{
// 		$this->assertEquals('test(comment)test@iana.org', $this->_object->parse('test(comment)test@iana.org')->__toString());
// 	}
	
// 	public function testParseEmailAddress130() : void
// 	{
// 		$this->assertEquals('test@(comment)[255.255.255.255]', $this->_object->parse('test@(comment)[255.255.255.255]')->__toString());
// 	}
	
// 	public function testParseEmailAddress131() : void
// 	{
// 		$this->assertEquals('(comment)abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghiklm@iana.org', $this->_object->parse('(comment)abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghiklm@iana.org')->__toString());
// 	}
	
// 	public function testParseEmailAddress132() : void
// 	{
// 		$this->assertEquals('test@(comment)abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl.com', $this->_object->parse('test@(comment)abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl.com')->__toString());
// 	}
	
// 	public function testParseEmailAddress133() : void
// 	{
// 		$this->assertEquals('(comment)test@abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghik.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghik.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijk.abcdefghijklmnopqrstuvwxyzabcdefghijk.abcdefghijklmnopqrstu', $this->_object->parse('(comment)test@abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghik.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghik.abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijk.abcdefghijklmnopqrstuvwxyzabcdefghijk.abcdefghijklmnopqrstu')->__toString());
// 	}
	
	public function testParseEmailAddress134() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("test@iana.org\x0A");
	}
	
	public function testParseEmailAddress135() : void
	{
		$this->assertEquals('test@xn--hxajbheg2az3al.xn--jxalpdlp', $this->_object->parse('test@xn--hxajbheg2az3al.xn--jxalpdlp')->__toString());
	}
	
	public function testParseEmailAddress136() : void
	{
		$this->assertEquals('xn--test@iana.org', $this->_object->parse('xn--test@iana.org')->__toString());
	}
	
	public function testParseEmailAddress137() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@iana.org-');
	}
	
	public function testParseEmailAddress138() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('"test@iana.org');
	}
	
	public function testParseEmailAddress139() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('(test@iana.org');
	}
	
	public function testParseEmailAddress140() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@(iana.org');
	}
	
	public function testParseEmailAddress141() : void
	{
		// autocorrected case
		$this->assertEquals('test@[1.2.3.4]', $this->_object->parse('test@[1.2.3.4'));
	}
	
	public function testParseEmailAddress142() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('"test\\"@iana.org');
	}
	
	public function testParseEmailAddress143() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('(comment\\)test@iana.org');
	}
	
	public function testParseEmailAddress144() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@iana.org(comment\\)');
	}
	
	public function testParseEmailAddress145() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@iana.org(comment\\');
	}
	
	public function testParseEmailAddress146() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[RFC-5322-domain-literal]');
	}
	
	public function testParseEmailAddress147() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[RFC-5322]-domain-literal]');
	}
	
	public function testParseEmailAddress148() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[RFC-5322-[domain-literal]');
	}
	
	public function testParseEmailAddress149() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("test@[RFC-5322-\x07-domain-literal]");
	}
	
	public function testParseEmailAddress150() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("test@[RFC-5322-\x09-domain-literal]");
	}
	
	public function testParseEmailAddress151() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[RFC-5322-\\]-domain-literal]');
	}
	
	public function testParseEmailAddress152() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[RFC-5322-domain-literal\\]');
	}
	
	public function testParseEmailAddress153() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[RFC-5322-domain-literal\\');
	}
	
	public function testParseEmailAddress154() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[RFC 5322 domain literal]');
	}
	
	public function testParseEmailAddress155() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[RFC-5322-domain-literal] (comment)');
	}
	
// 	public function testParseEmailAddress156() : void
// 	{
// 		$this->expectException(ParseException::class);
		
// 		$this->_object->parse("\x7f@iana.org");
// 	}
	
	public function testParseEmailAddress157() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("test@\x7f.org");
	}
	
	public function testParseEmailAddress158() : void
	{
		$this->assertEquals("\"\x7f\"@iana.org", $this->_object->parse("\"\x7f\"@iana.org")->__toString());
	}
	
// 	public function testParseEmailAddress159() : void
// 	{
// 		$this->assertEquals("\"\\\x7f\"@iana.org", $this->_object->parse("\"\\\x7f\"@iana.org")->__toString());
// 	}
	
// 	public function testParseEmailAddress160() : void
// 	{
// 		$this->assertEquals("(\x7f)test@iana.org", $this->_object->parse("(\x7f)test@iana.org")->__toString());
// 	}
	
	public function testParseEmailAddress161() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("test@iana.org\x0D");
	}
	
	public function testParseEmailAddress162() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("\x0Dtest@iana.org");
	}
	
	public function testParseEmailAddress163() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("\"\x0Dtest\"@iana.org");
	}
	
	public function testParseEmailAddress164() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("(\x0D)test@iana.org");
	}
	
	public function testParseEmailAddress165() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("test@iana.org(\x0D)");
	}
	
	public function testParseEmailAddress166() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("\x0Atest@iana.org");
	}
	
	public function testParseEmailAddress167() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("\"\x0A\"@iana.org");
	}
	
	public function testParseEmailAddress168() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("(\x0A)test@iana.org");
	}
	
// 	public function testParseEmailAddress169() : void
// 	{
// 		$this->expectException(ParseException::class);
		
// 		$this->_object->parse("\x07@iana.org");
// 	}
	
	public function testParseEmailAddress170() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("test@\x07.org");
	}
	
	public function testParseEmailAddress171() : void
	{
		$this->assertEquals("\"\x07\"@iana.org", $this->_object->parse("\"\x07\"@iana.org")->__toString());
	}
	
// 	public function testParseEmailAddress172() : void
// 	{
// 		$this->assertEquals("\"\\\x07\"@iana.org", $this->_object->parse("\"\\\x07\"@iana.org")->__toString());
// 	}
	
// 	public function testParseEmailAddress173() : void
// 	{
// 		$this->assertEquals("(\x07)test@iana.org", $this->_object->parse("(\x07)test@iana.org")->__toString());
// 	}
	
	public function testParseEmailAddress174() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("\x0D\x0Atest@iana.org");
	}
	
	public function testParseEmailAddress175() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("\x0D\x0A \x0D\x0Atest@iana.org");
	}
	
	public function testParseEmailAddress176() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse(" \x0D\x0Atest@iana.org");
	}
	
	public function testParseEmailAddress177() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse(" \x0D\x0A test@iana.org");
	}
	
	public function testParseEmailAddress178() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse(" \x0D\x0A \x0D\x0Atest@iana.org");
	}
	
	public function testParseEmailAddress179() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse(" \x0D\x0A\x0D\x0Atest@iana.org");
	}
	
	public function testParseEmailAddress180() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse(" \x0D\x0A\x0D\x0A test@iana.org");
	}
	
	public function testParseEmailAddress181() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("test@iana.org\x0D\x0A ");
	}
	
	public function testParseEmailAddress182() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("test@iana.org\x0D\x0A \x0D\x0A ");
	}
	
	public function testParseEmailAddress183() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("test@iana.org\x0D\x0A");
	}
	
	public function testParseEmailAddress184() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("test@iana.org\x0D\x0A \x0D\x0A");
	}
	
	public function testParseEmailAddress185() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("test@iana.org \x0D\x0A");
	}
	
	public function testParseEmailAddress186() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("test@iana.org \x0D\x0A ");
	}
	
	public function testParseEmailAddress187() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("test@iana.org \x0D\x0A \x0D\x0A");
	}
	
	public function testParseEmailAddress188() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("test@iana.org \x0D\x0A\x0D\x0A");
	}
	
	public function testParseEmailAddress189() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("test@iana.org \x0D\x0A\x0D\x0A ");
	}
	
	public function testParseEmailAddress190() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse(' test@iana.org');
	}
	
	public function testParseEmailAddress191() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@iana.org ');
	}
	
	public function testParseEmailAddress192() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@[IPv6:1::2:]');
	}
	
	public function testParseEmailAddress193() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse("\"test\xA9\"@iana.org");
	}
	
	public function testParseEmailAddress194() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('test@iana/icann.org');
	}
	
// 	public function testParseEmailAddress195() : void
// 	{
// 		$this->assertEquals('test.(comment)test@iana.org', $this->_object->parse('test.(comment)test@iana.org')->__toString());
// 	}
	
	public function testParseEmailAddress196() : void
	{
		$this->assertEquals('test@org', $this->_object->parse('test@org')->__toString());
	}
	
	public function testParseEmailAddress197() : void
	{
		$this->assertEquals('test@test.com', $this->_object->parse('test@test.com')->__toString());
	}
	
	public function testParseEmailAddress198() : void
	{
		$this->assertEquals('test@nic.no', $this->_object->parse('test@nic.no')->__toString());
	}
	
	// https://en.wikipedia.org/wiki/Email_address below
	
	public function testParseEmailAddress199() : void
	{
		$this->assertEquals('simple@example.com', $this->_object->parse('simple@example.com')->__toString());
	}
	
	public function testParseEmailAddress200() : void
	{
		$this->assertEquals('very.common@example.com', $this->_object->parse('very.common@example.com')->__toString());
	}
	
	public function testParseEmailAddress201() : void
	{
		$this->assertEquals('disposable.style.email.with+symbol@example.com', $this->_object->parse('disposable.style.email.with+symbol@example.com')->__toString());
	}
	
	public function testParseEmailAddress202() : void
	{
		$this->assertEquals('other.email-with-hyphen@example.com', $this->_object->parse('other.email-with-hyphen@example.com')->__toString());
	}
	
	public function testParseEmailAddress203() : void
	{
		$this->assertEquals('fully-qualified-domain@example.com', $this->_object->parse('fully-qualified-domain@example.com')->__toString());
	}
	
	public function testParseEmailAddress204() : void
	{
		$this->assertEquals('user.name+tag+sorting@example.com', $this->_object->parse('user.name+tag+sorting@example.com')->__toString());
	}
	
	public function testParseEmailAddress205() : void
	{
		$this->assertEquals('x@example.com', $this->_object->parse('x@example.com')->__toString());
	}
	
	public function testParseEmailAddress206() : void
	{
		$this->assertEquals('example-indeed@strange-example.com', $this->_object->parse('example-indeed@strange-example.com')->__toString());
	}
	
	public function testParseEmailAddress207() : void
	{
		// local domain name with no tld, allowed although ICANN discourages them
		$this->assertEquals('admin@mailserver1', $this->_object->parse('admin@mailserver1')->__toString());
	}
	
	public function testParseEmailAddress208() : void
	{
		$this->assertEquals('example@s.example', $this->_object->parse('example@s.example')->__toString());
	}
	
// 	public function testParseEmailAddress209() : void
// 	{
// 		$this->assertEquals('"\ "@example.org', $this->_object->parse('"\ "@example.org')->__toString());
// 	}
	
// 	public function testParseEmailAddress210() : void
// 	{
// 		$this->assertEquals('"john..doe"@example.org', $this->_object->parse('"john..doe"@example.org')->__toString());
// 	}
	
	public function testParseEmailAddress211() : void
	{
		$this->assertEquals('mailhost!username@example.org', $this->_object->parse('mailhost!username@example.org')->__toString());
	}
	
	public function testParseEmailAddress212() : void
	{
		$this->assertEquals('user%example.com@example.org', $this->_object->parse('user%example.com@example.org')->__toString());
	}
	
	public function testParseEmailAddress213() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('Abc.example.com');
	}
	
	public function testParseEmailAddress214() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('A@b@c@example.com');
	}
	
	public function testParseEmailAddress215() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('a"b(c)d,e:f;g<h>i[j\\k]l@example.com');
	}
	
	public function testParseEmailAddress216() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('just"not"right@example.com');
	}
	
	public function testParseEmailAddress217() : void
	{
		$this->assertEquals('" "@example.org', $this->_object->parse('" "@example.org')->__toString());
	}
	
	public function testParseEmailAddress218() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('this is"not\\allowed@example.com');
	}
	
// 	public function testParseEmailAddress219() : void
// 	{
// 		$this->expectException(ParseException::class);
		
// 		$this->_object->parse('this\ still\"not\\allowed@example.com');
// 	}
	
	public function testParseEmailAddress220() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('1234567890123456789012345678901234567890123456789012345678901234+x@example.com');
	}
	
	public function testParseEmailAddress221() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('i_like_underscore@but_its_not_allow_in_this_part.example.com');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new EmailAddressParser();
	}
	
}
