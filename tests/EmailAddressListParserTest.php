<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\Email\EmailAddress;
use PhpExtended\Email\EmailAddressList;
use PhpExtended\Email\EmailAddressListParser;
use PHPUnit\Framework\TestCase;

/**
 * EmailAddressListParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Email\EmailAddressListParser
 *
 * @internal
 *
 * @small
 */
class EmailAddressListParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var EmailAddressListParser
	 */
	protected EmailAddressListParser $_object;
	
	public function testParseEmptyString() : void
	{
		$this->assertEquals(new EmailAddressList([]), $this->_object->parse(null));
	}
	
	public function testParseEmailAddress() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('foo@example.com'));
	}
	
	public function testParseMultipleEmailAddresses() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
			new EmailAddress('bar', new Domain(['example', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('foo@example.com,bar@example.com'));
	}
	
	public function testParseMultipleEmailAddressesWithSpaceBefore() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
			new EmailAddress('bar', new Domain(['example', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->parse(' foo@example.com,bar@example.com'));
	}
	
	public function testParseMultipleEmailAddressesWithSpaceAfter() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
			new EmailAddress('bar', new Domain(['example', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('foo@example.com,bar@example.com '));
	}
	
	public function testParseMultipleEmailAddressesWithSpaceBetweenEnd() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
			new EmailAddress('bar', new Domain(['example', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('foo@example.com ,bar@example.com'));
	}
	
	public function testParseMultipleEmailAddressesWithSpacesBetweenStart() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
			new EmailAddress('bar', new Domain(['example', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('foo@example.com, bar@example.com'));
	}
	
	public function testParseMultipleEmailAddressesWithSpacesEverywhere() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
			new EmailAddress('bar', new Domain(['example', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->parse(' foo@example.com , bar@example.com '));
	}
	
	public function testParseEmptySlotBefore() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->parse(',foo@example.com'));
	}
	
	public function testParseEmptySlotBeforeWithSpaces() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->parse(' , foo@example.com'));
	}
	
	public function testParseEmptySlotAfter() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('foo@example.com,'));
	}
	
	public function testParseEmptySlotAfterWithSpaces() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('foo@example.com , '));
	}
	
	public function testParseDoubleComma() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
			new EmailAddress('bar', new Domain(['example', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('foo@example.com,,bar@example.com'));
	}
	
	public function testParseDoubleCommaWithSpaces() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foo', new Domain(['example', 'com'])),
			new EmailAddress('bar', new Domain(['example', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('foo@example.com , , bar@example.com'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new EmailAddressListParser();
	}
	
}
