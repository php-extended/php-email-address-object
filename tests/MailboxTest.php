<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\Email\EmailAddress;
use PhpExtended\Email\Mailbox;
use PHPUnit\Framework\TestCase;

/**
 * MailboxTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Email\Mailbox
 *
 * @internal
 *
 * @small
 */
class MailboxTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Mailbox
	 */
	protected Mailbox $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('"Dr. Hyde" <foobar@example.com>', $this->_object->__toString());
	}
	
	public function testGetDisplayName() : void
	{
		$this->assertEquals('Dr. Hyde', $this->_object->getDisplayName());
	}
	
	public function testWithDisplayName() : void
	{
		$this->assertEquals('"Mr. Jeykill" <foobar@example.com>', $this->_object->withDisplayName('Mr. Jeykill')->__toString());
	}
	
	public function testGetEmailAddress() : void
	{
		$this->assertEquals(new EmailAddress('foobar', new Domain(['example', 'com'])), $this->_object->getEmailAddress());
	}
	
	public function testWithEmailAddress() : void
	{
		$this->assertEquals('"Dr. Hyde" <quux@example.com>', $this->_object->withEmailAddress(new EmailAddress('quux', new Domain(['example', 'com'])))->__toString());
	}
	
	public function testWithEmailAddressLocalPart() : void
	{
		$this->assertEquals('"Dr. Hyde" <quuxbar@example.com>', $this->_object->withEmailAddressLocalPart('quuxbar')->__toString());
	}
	
	public function testWithEmailAddressDomain() : void
	{
		$this->assertEquals('"Dr. Hyde" <foobar@quuxbar.com>', $this->_object->withEmailAddressDomain(new Domain('quuxbar.com'))->__toString());
	}
	
	public function testGetCanonicalRepresentation() : void
	{
		$this->assertEquals('"Dr. Hyde" <foobar@example.com>', $this->_object->getCanonicalRepresentation());
	}
	
	public function testGetCanonicalRepresentationDisplayless() : void
	{
		$this->assertEquals('foobar@example.com', (new Mailbox(new EmailAddress('foobar', new Domain(['example', 'com']))))->getCanonicalRepresentation());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals($this->_object));
	}
	
	public function testContainsEmailAddress() : void
	{
		$this->assertTrue($this->_object->containsEmailAddress(new EmailAddress('foobar', new Domain(['example', 'com']))));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Mailbox(new EmailAddress('foobar', new Domain(['example', 'com'])), 'Dr. Hyde');
	}
	
}
