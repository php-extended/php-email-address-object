<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Email\MailboxGroupList;
use PhpExtended\Email\MailboxGroupListParser;
use PHPUnit\Framework\TestCase;

/**
 * MailboxGroupListParser class file.
 *
 * @author Anastaszor
 * @covers \PhpExtended\Email\MailboxGroupListParser
 *
 * @internal
 *
 * @small
 */
class MailboxGroupListParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 *
	 * @var MailboxGroupListParser
	 */
	protected MailboxGroupListParser $_object;
	
	public function testParseEmptyString() : void
	{
		$this->assertEquals(new MailboxGroupList([]), $this->_object->parse(null));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MailboxGroupListParser();
	}
	
}
