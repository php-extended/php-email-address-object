<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\Email\EmailAddress;
use PhpExtended\Email\EmailAddressList;
use PHPUnit\Framework\TestCase;

/**
 * EmailAddressListTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Email\EmailAddressList
 *
 * @internal
 *
 * @small
 */
class EmailAddressListTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var EmailAddressList
	 */
	protected EmailAddressList $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('foobar@example.com, barfoo@example2.com', $this->_object->__toString());
	}
	
	public function testCount() : void
	{
		$this->assertEquals(2, $this->_object->count());
	}
	
	public function testIteration() : void
	{
		$k = 0;
		
		foreach($this->_object as $key => $email)
		{
			$this->assertIsInt($key);
			$this->assertInstanceOf(EmailAddress::class, $email);
			$k++;
		}
		
		$this->assertEquals(2, $k);
	}
	
	public function testWithDomain() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foobar', new Domain(['example', 'com'])),
			new EmailAddress('barfoo', new Domain(['example', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->withDomain(new Domain(['example', 'com'])));
	}
	
	public function testFilterByDomain() : void
	{
		$expected = new EmailAddressList([
			new EmailAddress('foobar', new Domain(['example', 'com'])),
		]);
		
		$this->assertEquals($expected, $this->_object->filterByDomain(new Domain(['example', 'com'])));
	}
	
	public function testWithEmailAddress() : void
	{
		$this->_object = $this->_object->withEmailAddress(new EmailAddress('quux', new Domain(['example', 'com'])));
		
		$this->assertEquals('foobar@example.com, barfoo@example2.com, quux@example.com', $this->_object->__toString());
	}
	
	public function testWithEmailAddressRedundant() : void
	{
		$this->_object = $this->_object->withEmailAddress(new EmailAddress('foobar', new Domain(['example', 'com'])));
		
		$this->assertEquals('foobar@example.com, barfoo@example2.com', $this->_object->__toString());
	}
	
	public function testWithEmailAddressList() : void
	{
		$this->_object = $this->_object->withEmailAddressList(new EmailAddressList([
			new EmailAddress('quux', new Domain(['example', 'com'])),
			new EmailAddress('foobar', new Domain(['example', 'com'])),
		]));
		
		$this->assertEquals('foobar@example.com, barfoo@example2.com, quux@example.com', $this->_object->__toString());
	}
	
	public function testGetCanonicalRepresentation() : void
	{
		$this->assertEquals('foobar@example.com, barfoo@example2.com', $this->_object->getCanonicalRepresentation());
	}
	
	public function testIsEmpty() : void
	{
		$this->assertFalse($this->_object->isEmpty());
	}
	
	public function testCollectDomains() : void
	{
		$expected = new ArrayIterator([
			new Domain(['example', 'com']),
			new Domain(['example2', 'com']),
		]);
		
		$this->assertEquals($expected, $this->_object->collectDomains());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals($this->_object));
	}
	
	public function testNotEqualsNull() : void
	{
		$this->assertFalse($this->_object->equals(null));
	}
	
	public function testNotEqualsOther() : void
	{
		$this->assertFalse($this->_object->equals(new EmailAddressList([
			new EmailAddress('barfoo', new Domain(['example2', 'com'])),
		])));
	}
	
	public function testContainsEmailAddress() : void
	{
		$this->assertTrue($this->_object->containsEmailAddress(new EmailAddress('foobar', new Domain(['example', 'com']))));
	}
	
	public function testDoesNotContainEmailAddress() : void
	{
		$this->assertFalse($this->_object->containsEmailAddress(new EmailAddress('quux', new Domain(['example2', 'com']))));
	}
	
	public function testContainsEmailAddressList() : void
	{
		$this->assertTrue($this->_object->containsEmailAddressList(new EmailAddressList([
			new EmailAddress('barfoo', new Domain(['example2', 'com'])),
			new EmailAddress('foobar', new Domain(['example', 'com'])),
		])));
	}
	
	public function testDoesNotContainEmailAddressList() : void
	{
		$this->assertFalse($this->_object->containsEmailAddressList(new EmailAddressList([
			new EmailAddress('foobar', new Domain(['example', 'com'])),
			new EmailAddress('quux', new Domain(['example', 'com'])),
		])));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new EmailAddressList([
			new EmailAddress('foobar', new Domain(['example', 'com'])),
			new EmailAddress('barfoo', new Domain(['example2', 'com'])),
		]);
	}
	
}
