<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\Email\EmailAddress;
use PhpExtended\Ip\Ipv4Address;
use PhpExtended\Ip\Ipv6Address;
use PHPUnit\Framework\TestCase;

/**
 * EmailAddressTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Email\EmailAddress
 *
 * @internal
 *
 * @small
 */
class EmailAddressTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var EmailAddress
	 */
	protected EmailAddress $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('"test(joy)test"@example.com', $this->_object->__toString());
	}
	
	public function testNoDomainProvided() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		new EmailAddress('local', null);
	}
	
	public function testDomainIpv4Provided() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		new EmailAddress('local', new Domain(['example', 'com']), new Ipv4Address(127, 0, 0, 1));
	}
	
	public function testDomainIpv6Provided() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		new EmailAddress('local', new Domain(['example', 'com']), null, new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 1));
	}
	
	public function testIpv4Ipv6Provided() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		new EmailAddress('local', null, new Ipv4Address(127, 0, 0, 1), new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 1));
	}
	
	public function testHasIpAsDomain() : void
	{
		$this->assertFalse($this->_object->hasIpAsDomain());
	}
	
	public function testHasIpAsDomainWithIpv4() : void
	{
		$this->assertTrue((new EmailAddress('local', null, new Ipv4Address(127, 0, 0, 1)))->hasIpAsDomain());
	}
	
	public function testHasIpAsDomainWithIpv6() : void
	{
		$this->assertTrue((new EmailAddress('local', null, null, new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 1)))->hasIpAsDomain());
	}
	
	public function testWithLocalPart() : void
	{
		$this->assertEquals('test@example.com', $this->_object->withLocalPart('test')->getCanonicalRepresentation());
	}
	
	public function testGetLocalPart() : void
	{
		$this->assertEquals('test(joy)test', $this->_object->getLocalPart());
	}
	
	public function testWithTruncatedLocalPart() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object->withLocalPart('');
	}
	
	public function testWithLongLocalPart() : void
	{
		$this->assertEquals('azertyuiopqsdfghjklmwxcvbnazertyuiopqsdfghjklmwxcvbnazertyuiopqs@example.com', $this->_object->withLocalPart('azertyuiopqsdfghjklmwxcvbnazertyuiopqsdfghjklmwxcvbnazertyuiopqs')->__toString());
	}
	
	public function testWithTooLongLocalPart() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object->withLocalPart('azertyuiopqsdfghjklmwxcvbnazertyuiopqsdfghjklmwxcvbnazertyuiopqst');
	}
	
	public function testWithUnusualLocalPart() : void
	{
		$this->assertEquals('"this.is unusual"@example.com', $this->_object->withLocalPart('this."is\\ unusual"')->__toString());
	}
	
	public function testWithDomain() : void
	{
		$this->assertEquals('"test(joy)test"@toto.com', $this->_object->withDomain(new Domain(['toto', 'com']))->getCanonicalRepresentation());
	}
	
	public function testWithTooLongOverall() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object = $this->_object->withLocalPart('abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghiklm');
		$this->_object = $this->_object->withDomain(new Domain(['abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl', 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl', 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghikl']));
	}
	
	public function testWithDomainIpAddress() : void
	{
		$this->assertEquals('"test(joy)test"@[127.0.0.1]', $this->_object->withIpv4Address(new Ipv4Address(127, 0, 0, 1))->__toString());
	}
	
	public function testWithDomainIpv6Address() : void
	{
		$this->assertEquals('"test(joy)test"@[IPV6:0000:0000:0000:0000:0000:0000:0000:0001]', $this->_object->withIpv6Address(new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 1))->__toString());
	}
	
	public function testGetDomain() : void
	{
		$this->assertEquals(new Domain(['example', 'com']), $this->_object->getDomain());
	}
	
	public function testGetDomainWithIpv4() : void
	{
		$this->assertEquals('1.0.0.127.in-addr.arpa', (new EmailAddress('test', null, new Ipv4Address(127, 0, 0, 1)))->getDomain()->getCanonicalRepresentation());
	}
	
	public function testGetDomainWithIpv6() : void
	{
		$this->assertEquals('1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa', (new EmailAddress('test', null, null, new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 1)))->getDomain()->getCanonicalRepresentation());
	}
	
	public function testGetIpv4() : void
	{
		$expected = new Ipv4Address(127, 0, 0, 1);
		$this->assertEquals($expected, (new EmailAddress('test', null, $expected))->getIpv4Address());
	}
	
	public function testGetIpv6() : void
	{
		$expected = new Ipv6Address(0, 0, 0, 0, 0, 0, 0, 1);
		$this->assertEquals($expected, (new EmailAddress('test', null, null, $expected))->getIpv6Address());
	}
	
	public function testGetCanonicalRepresentation() : void
	{
		$this->assertEquals('"test(joy)test"@example.com', $this->_object->getCanonicalRepresentation());
	}
	
	public function testEqualsSame() : void
	{
		$this->assertTrue($this->_object->equals($this->_object));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new EmailAddress('test(joy)test', new Domain(['example', 'com']));
	}
	
}
