<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Email\MailboxGroup;
use PhpExtended\Email\MailboxGroupParser;
use PhpExtended\Email\MailboxList;
use PHPUnit\Framework\TestCase;

/**
 * MailboxGroupParser class file.
 *
 * @author Anastaszor
 * @covers \PhpExtended\Email\MailboxGroupParser
 *
 * @internal
 *
 * @small
 */
class MailboxGroupParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 *
	 * @var MailboxGroupParser
	 */
	protected MailboxGroupParser $_object;
	
	public function testParseEmptyString() : void
	{
		$this->assertEquals(new MailboxGroup(new MailboxList([])), $this->_object->parse(null));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MailboxGroupParser();
	}
	
}
