<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\Email\EmailAddress;
use PhpExtended\Email\Mailbox;
use PhpExtended\Email\MailboxParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * MailboxParser class file.
 *
 * @author Anastaszor
 * @covers \PhpExtended\Email\MailboxParser
 *
 * @internal
 *
 * @small
 */
class MailboxParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 *
	 * @var MailboxParser
	 */
	protected MailboxParser $_object;
	
	public function testParseEmptyString() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse(null);
	}
	
	public function testParseTestUser() : void
	{
		$expected = new Mailbox(new EmailAddress('t.user', new Domain(['example', 'com'])), 'Test User');
		$this->assertEquals($expected, $this->_object->parse('Test User <t.user@example.com>'));
	}
	
	public function testParseQuotedUser() : void
	{
		$expected = new Mailbox(new EmailAddress('test.user', new Domain(['example', 'com'])), 'Test User');
		$this->assertEquals($expected, $this->_object->parse('"Test User" <test.user@example.com>'));
	}
	
	public function testParseUnquotedQuotedUser() : void
	{
		$expected = new Mailbox(new EmailAddress('test.user', new Domain(['example', 'com'])), 'Test "User"');
		$this->assertEquals($expected, $this->_object->parse('Test "User" <test.user@example.com>'));
	}
	
	public function testParseQuotedUnquotedUser() : void
	{
		$expected = new Mailbox(new EmailAddress('test.user', new Domain(['example', 'com'])), '"Test" User');
		$this->assertEquals($expected, $this->_object->parse('"Test" User <test.user@example.com>'));
	}
	
	public function testParseUnquotedQuotedUnquotedUser() : void
	{
		$expected = new Mailbox(new EmailAddress('test.user', new Domain(['example', 'com'])), 'Test "Re" User');
		$this->assertEquals($expected, $this->_object->parse('Test "Re" User <test.user@example.com>'));
	}
	
	public function testParseSpaceUserName() : void
	{
		$expected = new Mailbox(new EmailAddress('test.user', new Domain(['example', 'com'])), ' ');
		$this->assertEquals($expected, $this->_object->parse('" " <test.user@example.com>'));
	}
	
	public function testParseSpaceBeforeUserName() : void
	{
		$expected = new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc');
		$this->assertEquals($expected, $this->_object->parse(' "Foo, Inc" <foo@example.com>'));
	}
	
	public function testParseSpaceAfterEmailAddress() : void
	{
		$expected = new Mailbox(new EmailAddress('foo', new Domain(['example', 'com'])), 'Foo, Inc');
		$this->assertEquals($expected, $this->_object->parse('"Foo, Inc" <foo@example.com> '));
	}
	
	public function testParseNoUserName() : void
	{
		$expected = new Mailbox(new EmailAddress('test.user', new Domain(['example', 'com'])));
		$this->assertEquals($expected, $this->_object->parse('test.user@example.com'));
	}
	
	public function testParseNoUserNameWithBrackets() : void
	{
		$expected = new Mailbox(new EmailAddress('test.user', new Domain(['example', 'com'])));
		$this->assertEquals($expected, $this->_object->parse('<test.user@example.com>'));
	}
	
	public function testParseNoUserNameWithMissingBeginBracket() : void
	{
		$expected = new Mailbox(new EmailAddress('test.user', new Domain(['example', 'com'])));
		$this->assertEquals($expected, $this->_object->parse('test.user@example.com>'));
	}
	
	public function testParseNoUserNameWithMissingEndingBracket() : void
	{
		$expected = new Mailbox(new EmailAddress('test.user', new Domain(['example', 'com'])));
		$this->assertEquals($expected, $this->_object->parse('<test.user@example.com'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MailboxParser();
	}
	
}
