<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use InvalidArgumentException;
use LengthException;
use PhpExtended\Domain\Domain;
use PhpExtended\Lexer\LexerConfiguration;
use PhpExtended\Lexer\LexerFactory;
use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParser;

/**
 * MailboxGroupListParser class file.
 * 
 * This class is a simple implementation of the EmailAddressParserInterface.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\AbstractParser<MailboxGroupListInterface>
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class MailboxGroupListParser extends AbstractParser implements MailboxGroupListParserInterface
{
	
	public const L_ALNUM = 1; // [a-zA-Z0-9]
	public const L_SYMBOLS = 2; // !#$%&'*+-/?^_`{|}~
	public const L_WHITESPACE = 3; // sp
	public const L_DOT = 4; // .
	public const L_OPEN_PARENTHS = 5; // (
	public const L_CLOS_PARENTHS = 6; // )
	public const L_OPEN_TBRACKET = 7; // <
	public const L_CLOS_TBRACKET = 8; // >
	public const L_OPEN_SBRACKET = 9; // [
	public const L_CLOS_SBRACKET = 10; // ]
	public const L_FULL_COLON = 11; // :
	public const L_SEMI_COLON = 12; // ;
	public const L_AROBASE = 13; // @
	public const L_ANTISLASH = 14; // \
	public const L_COMMA = 15; // ,
	public const L_DOUBLE_QUOTE = 16; // "
	public const T_ATOM = 17; // abc
	public const T_ATOM_DOT = 18; // abc.
	public const T_QTEXT = 19; // "abc
	public const T_QTEXT_ESC = 20; // "abc\
	public const T_QTEXT_COMPL = 21; // "abc"
	public const T_EMAIL_ARB = 22; // abc@    // "abc"@
	public const T_EMAIL_DOM = 23; // abc@b
	public const T_EMAIL_DOM_DOT = 24; // abc@b.
	public const T_EMAIL_COMPL = 25; // abc@b.c
	public const T_COMM_BUILDING = 26; // (a
	public const T_COMM_FINISHED = 27; // (a)
	
	/**
	 * The lexer factory.
	 * 
	 * @var LexerFactory
	 */
	protected LexerFactory $_lexerFactory;
	
	/**
	 * The lexer configuration.
	 * 
	 * @var LexerConfiguration
	 */
	protected LexerConfiguration $_lexerConfig;
	
	/**
	 * Builds a new MailboxGroupListParser with its configuration.
	 */
	public function __construct()
	{
		$this->_lexerFactory = new LexerFactory();
		$this->_lexerConfig = new LexerConfiguration();
		$this->_lexerConfig->addMappings(LexerInterface::CLASS_ALNUM, self::L_ALNUM);
		$this->_lexerConfig->addMappings("!#$%&'*+-/?^_`{|}~", self::L_SYMBOLS);
		$this->_lexerConfig->addMappings(LexerInterface::CLASS_SPACE, self::L_WHITESPACE);
		$this->_lexerConfig->addMappings('.', self::L_DOT);
		$this->_lexerConfig->addMappings('(', self::L_OPEN_PARENTHS);
		$this->_lexerConfig->addMappings(')', self::L_CLOS_PARENTHS);
		$this->_lexerConfig->addMappings('<', self::L_OPEN_TBRACKET);
		$this->_lexerConfig->addMappings('>', self::L_CLOS_TBRACKET);
		$this->_lexerConfig->addMappings('[', self::L_OPEN_SBRACKET);
		$this->_lexerConfig->addMappings(']', self::L_CLOS_SBRACKET);
		$this->_lexerConfig->addMappings(':', self::L_FULL_COLON);
		$this->_lexerConfig->addMappings(';', self::L_SEMI_COLON);
		$this->_lexerConfig->addMappings('@', self::L_AROBASE);
		$this->_lexerConfig->addMappings('\\', self::L_ANTISLASH);
		$this->_lexerConfig->addMappings(',', self::L_COMMA);
		$this->_lexerConfig->addMappings('"', self::L_DOUBLE_QUOTE);
		
		$this->_lexerConfig->addMerging(self::L_WHITESPACE, self::L_WHITESPACE, self::L_WHITESPACE);
		$this->_lexerConfig->addMerging(self::L_ALNUM, self::L_ALNUM, self::T_ATOM);
		$this->_lexerConfig->addMerging(self::L_SYMBOLS, self::L_SYMBOLS, self::T_ATOM);
		$this->_lexerConfig->addMerging(self::T_ATOM, self::L_ALNUM, self::T_ATOM);
		$this->_lexerConfig->addMerging(self::T_ATOM, self::L_SYMBOLS, self::T_ATOM);
		$this->_lexerConfig->addMerging(self::T_ATOM, self::L_DOT, self::T_ATOM_DOT);
		$this->_lexerConfig->addMerging(self::T_ATOM_DOT, self::L_ALNUM, self::T_ATOM);
		$this->_lexerConfig->addMerging(self::T_ATOM_DOT, self::L_SYMBOLS, self::T_ATOM);
		
		$this->_lexerConfig->addMerging(self::L_DOUBLE_QUOTE, self::L_ANTISLASH, self::T_QTEXT_ESC);
		$this->_lexerConfig->addMerging(self::L_DOUBLE_QUOTE, self::L_DOUBLE_QUOTE, self::T_QTEXT_COMPL);
		$this->_lexerConfig->addMerging(self::L_DOUBLE_QUOTE, LexerInterface::L_EVERYTHING_ELSE, self::T_QTEXT);
		$this->_lexerConfig->addMerging(self::T_QTEXT, self::L_ANTISLASH, self::T_QTEXT_ESC);
		$this->_lexerConfig->addMerging(self::T_QTEXT, self::L_DOUBLE_QUOTE, self::T_QTEXT_COMPL);
		$this->_lexerConfig->addMerging(self::T_QTEXT, LexerInterface::L_EVERYTHING_ELSE, self::T_QTEXT);
		$this->_lexerConfig->addMerging(self::T_QTEXT_ESC, LexerInterface::L_EVERYTHING_ELSE, self::T_QTEXT);
		
		$this->_lexerConfig->addMerging(self::L_ALNUM, self::L_AROBASE, self::T_EMAIL_ARB);
		$this->_lexerConfig->addMerging(self::L_SYMBOLS, self::L_AROBASE, self::T_EMAIL_ARB);
		$this->_lexerConfig->addMerging(self::T_ATOM, self::L_AROBASE, self::T_EMAIL_ARB);
		$this->_lexerConfig->addMerging(self::T_ATOM_DOT, self::L_AROBASE, self::T_EMAIL_ARB);
		$this->_lexerConfig->addMerging(self::T_QTEXT_COMPL, self::L_AROBASE, self::T_EMAIL_ARB);
		$this->_lexerConfig->addMerging(self::T_EMAIL_ARB, self::L_ALNUM, self::T_EMAIL_DOM);
		$this->_lexerConfig->addMerging(self::T_EMAIL_DOM, self::L_ALNUM, self::T_EMAIL_DOM);
		$this->_lexerConfig->addMerging(self::T_EMAIL_DOM, self::L_DOT, self::T_EMAIL_DOM_DOT);
		$this->_lexerConfig->addMerging(self::T_EMAIL_DOM_DOT, self::L_ALNUM, self::T_EMAIL_COMPL);
		$this->_lexerConfig->addMerging(self::T_EMAIL_COMPL, self::L_DOT, self::T_EMAIL_DOM_DOT);
		$this->_lexerConfig->addMerging(self::T_EMAIL_COMPL, self::L_ALNUM, self::T_EMAIL_COMPL);
		
		$this->_lexerConfig->addMerging(self::L_OPEN_PARENTHS, self::L_CLOS_PARENTHS, self::T_COMM_FINISHED);
		$this->_lexerConfig->addMerging(self::L_OPEN_PARENTHS, LexerInterface::L_EVERYTHING_ELSE, self::T_COMM_BUILDING);
		$this->_lexerConfig->addMerging(self::T_COMM_BUILDING, self::L_CLOS_PARENTHS, self::T_COMM_FINISHED);
		$this->_lexerConfig->addMerging(self::T_COMM_BUILDING, LexerInterface::L_EVERYTHING_ELSE, self::T_COMM_BUILDING);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressParserInterface::parseString()
	 * @throws InvalidArgumentException
	 * @throws LengthException
	 */
	public function parse(?string $data) : MailboxGroupListInterface
	{
		return $this->parseLexer($this->_lexerFactory->createFromString((string) $data, $this->_lexerConfig));
	}
	
	/**
	 * Parses the data that comes from the given lexer.
	 * 
	 * @param LexerInterface $lexer
	 * @return MailboxGroupListInterface
	 * @throws InvalidArgumentException
	 * @throws LengthException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	protected function parseLexer(LexerInterface $lexer) : MailboxGroupListInterface
	{
		$mailboxGroupList = new MailboxGroupList([]);
		/** @var MailboxGroupInterface $currentMailboxGroup */
		$currentMailboxGroup = null;
		/** @var MailboxInterface $currentMailbox */
		$currentMailbox = null;
		/** @var ?string $currentAtom */
		$currentAtom = null;
		
		$lexer->rewind();
		
		while($lexer->valid())
		{
			/** @var \PhpExtended\Lexer\Lexeme $token */
			$token = $lexer->current();
			
			switch($token->getCode())
			{
				case self::L_ALNUM:
				case self::L_SYMBOLS:
					if(null === $currentAtom)
					{
						$currentAtom = '';
					}
					$currentAtom .= $token->getData();
					break;
					
				case self::L_FULL_COLON:
					// delimiter for mailbox group
					if(null !== $currentMailboxGroup)
					{
						$mailboxGroupList = $mailboxGroupList->withMailboxGroup($currentMailboxGroup);
					}
					$currentMailboxGroup = new MailboxGroup(new MailboxList([]), $currentAtom);
					$currentMailbox = null;
					$currentAtom = null;
					break;
					
				case self::L_SEMI_COLON:
					// delimiter for mailbox group
					if(null !== $currentMailbox && null !== $currentMailboxGroup)
					{
						$currentMailboxGroup = $currentMailboxGroup->withMailbox($currentMailbox);
					}
					if(null !== $currentMailbox && null === $currentMailboxGroup)
					{
						$currentMailboxGroup = new MailboxGroup(new MailboxList([$currentMailbox]));
					}
					if(null !== $currentMailboxGroup)
					{
						$mailboxGroupList = $mailboxGroupList->withMailboxGroup($currentMailboxGroup);
					}
					$currentMailboxGroup = null;
					$currentMailbox = null;
					$currentAtom = null;
					break;
					
				case self::L_COMMA:
					$currentMailbox = null;
					$currentAtom = null;
					break;
					
				case self::T_ATOM:
				case self::T_ATOM_DOT:
					$data = $token->getData();
					if('.' === $data[(int) \mb_strlen($data) - 1])
					{
						$data = (string) \mb_substr($data, 0, -1);
					}
					if(null !== $currentAtom)
					{
						$data = $currentAtom.' '.$data;
					}
					$currentAtom = $data;
					break;
					
				case self::T_QTEXT:
					$data = $token->getData();
					if('"' === $data[0])
					{
						$data = (string) \mb_substr($data, 1);
					}
					$currentAtom = $data;
					break;
					
				case self::T_QTEXT_ESC:
					$data = $token->getData();
					if('\\' === $data[(int) \mb_strlen($data) - 1])
					{
						$data = (string) \mb_substr($data, 0, -1);
					}
					if('"' === $data[0])
					{
						$data = (string) \mb_substr($data, 1);
					}
					$currentAtom = $data;
					break;
					
				case self::T_QTEXT_COMPL:
					$data = $token->getData();
					if('"' === $data[(int) \mb_strlen($data) - 1])
					{
						$data = (string) \mb_substr($data, 0, -1);
					}
					if('"' === $data[0])
					{
						$data = (string) \mb_substr($data, 1);
					}
					$currentAtom = $data;
					break;
					
				case self::T_EMAIL_ARB:
					$data = $token->getData();
					if('@' === $data[(int) \mb_strlen($data)])
					{
						$data = (string) \mb_substr($data, 0, -1);
					}
					$currentAtom = $data;
					break;
					
				case self::T_EMAIL_DOM_DOT:
					$data = $token->getData();
					if('.' === $data[(int) \mb_strlen($data) - 1])
					{
						$data = (string) \mb_substr($data, 0, -1);
					}
					if(null === $currentMailboxGroup)
					{
						$currentMailboxGroup = new MailboxGroup(new MailboxList([]));
					}
					if(null === $currentMailbox)
					{
						$local = $data;
						$domain = '';
						$arbpos = \mb_strrpos($data, '@');
						if(false !== $arbpos)
						{
							$local = (string) \mb_substr($data, 0, $arbpos);
							$domain = (string) \mb_substr($data, 1 + $arbpos);
						}
						$currentMailbox = new Mailbox(new EmailAddress($local, new Domain(\explode('.', $domain))));
					}
					$currentMailboxGroup = $currentMailboxGroup->withMailbox($currentMailbox);
					break;
					
				case self::T_EMAIL_DOM:
				case self::T_EMAIL_COMPL:
					$data = $token->getData();
					if(null === $currentMailboxGroup)
					{
						$currentMailboxGroup = new MailboxGroup(new MailboxList([]));
					}
					$local = $data;
					$domain = '';
					$arbpos = \mb_strrpos($data, '@');
					if(false !== $arbpos)
					{
						$local = (string) \mb_substr($data, 0, $arbpos);
						$domain = (string) \mb_substr($data, 1 + $arbpos);
					}
					$currentMailbox = new Mailbox(new EmailAddress($local, new Domain(\explode('.', $domain))), $currentAtom);
					$currentAtom = null;
					$currentMailboxGroup = $currentMailboxGroup->withMailbox($currentMailbox);
					break;
					
				case self::L_DOT:
				case self::L_OPEN_PARENTHS:
				case self::L_CLOS_PARENTHS:
				case self::L_OPEN_TBRACKET:
				case self::L_CLOS_TBRACKET:
				case self::L_OPEN_SBRACKET:
				case self::L_CLOS_SBRACKET:
				case self::L_AROBASE:
				case self::L_ANTISLASH:
				case self::L_DOUBLE_QUOTE:
				case self::T_COMM_BUILDING:
				case self::T_COMM_FINISHED:
				default:
					// ignore
			}
			
			$lexer->next();
		}
		
		if(null !== $currentMailbox)
		{
			if(null !== $currentMailboxGroup)
			{
				$currentMailboxGroup = $currentMailboxGroup->withMailbox($currentMailbox);
			}
			
			if(null === $currentMailboxGroup)
			{
				$mailboxGroupList = $mailboxGroupList->withMailbox($currentMailbox);
			}
		}
		
		if(null !== $currentMailboxGroup)
		{
			$mailboxGroupList = $mailboxGroupList->withMailboxGroup($currentMailboxGroup);
		}
		
		return $mailboxGroupList;
	}
	
}
