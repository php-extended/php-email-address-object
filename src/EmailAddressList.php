<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use ArrayIterator;
use Iterator;
use PhpExtended\Domain\DomainInterface;

/**
 * EmailAddressList with the given class file.
 * 
 * This class is a simple implementation of the EmailAddressListInterface
 * with anti duplication of email addresses.
 * 
 * @author Anastaszor
 */
class EmailAddressList implements EmailAddressListInterface
{
	
	/**
	 * The store of email addresses.
	 * 
	 * @var array<integer, EmailAddressInterface>
	 */
	protected array $_emails = [];
	
	/**
	 * Builds a new Email Address List with the given array of email addresses.
	 * 
	 * @param array<integer, EmailAddressInterface> $list
	 */
	public function __construct(array $list = [])
	{
		foreach($list as $email)
		{
			$this->addEmailAddress($email);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getCanonicalRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_emails);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function current() : EmailAddressInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return \current($this->_emails);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		\next($this->_emails);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return (int) \key($this->_emails);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return false !== \current($this->_emails);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		\reset($this->_emails);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressListInterface::withDomain()
	 */
	public function withDomain(DomainInterface $domain) : EmailAddressListInterface
	{
		$list = new EmailAddressList();
		
		foreach($this->_emails as $emailAddress)
		{
			$list->addEmailAddress($emailAddress->withDomain($domain));
		}
		
		return $list;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressListInterface::filterByDomain()
	 */
	public function filterByDomain(DomainInterface $domain) : EmailAddressListInterface
	{
		$canonical = $domain->getCanonicalRepresentation();
		$list = new EmailAddressList();
		
		foreach($this->_emails as $emailAddress)
		{
			if($emailAddress->getDomain()->getCanonicalRepresentation() === $canonical)
			{
				$list->addEmailAddress($emailAddress);
			}
		}
		
		return $list;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressListInterface::withEmailAddress()
	 */
	public function withEmailAddress(EmailAddressInterface $address) : EmailAddressListInterface
	{
		return (new EmailAddressList($this->_emails))->addEmailAddress($address);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressListInterface::withEmailAddressList()
	 */
	public function withEmailAddressList(EmailAddressListInterface $addressList) : EmailAddressListInterface
	{
		$eal = new EmailAddressList($this->_emails);
		
		foreach($addressList as $email)
		{
			$eal->addEmailAddress($email);
		}
		
		return $eal;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressListInterface::getCanonicalRepresentation()
	 */
	public function getCanonicalRepresentation() : string
	{
		$str = [];
		
		foreach($this->_emails as $email)
		{
			$str[] = $email->getCanonicalRepresentation();
		}
		
		return \implode(', ', $str);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressListInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return empty($this->_emails);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressListInterface::collectDomains()
	 */
	public function collectDomains() : Iterator
	{
		$domains = [];
		
		foreach($this->_emails as $emailAddress)
		{
			$domains[$emailAddress->getDomain()->getCanonicalRepresentation()] = $emailAddress->getDomain();
		}
		
		return new ArrayIterator(\array_values($domains));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressListInterface::equals()
	 */
	public function equals($other) : bool
	{
		if(!$other instanceof EmailAddressListInterface)
		{
			return false;
		}
		
		if($this->count() !== $other->count())
		{
			return false;
		}
		
		return $this->containsEmailAddressList($other)
			&& $other->containsEmailAddressList($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressListInterface::containsEmailAddress()
	 */
	public function containsEmailAddress(EmailAddressInterface $address) : bool
	{
		foreach($this->_emails as $emailAddress)
		{
			if($emailAddress->equals($address))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressListInterface::containsEmailAddressList()
	 */
	public function containsEmailAddressList(EmailAddressListInterface $addressList) : bool
	{
		foreach($addressList as $emailAddress)
		{
			if(!$this->containsEmailAddress($emailAddress))
			{
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Adds the given email address to this list.
	 * 
	 * @param EmailAddressInterface $email
	 * @return EmailAddressListInterface
	 */
	protected function addEmailAddress(EmailAddressInterface $email) : EmailAddressListInterface
	{
		foreach($this->_emails as $current)
		{
			if($current->equals($email))
			{
				return $this; // already present, anti duplicate
			}
		}
		
		$this->_emails[] = $email;
		
		return $this;
	}
	
}
