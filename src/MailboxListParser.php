<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;

/**
 * MailboxListParser class file.
 * 
 * This class is a simple implementation of the MailboxListParserInterface.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\AbstractParserLexer<MailboxListInterface>
 */
class MailboxListParser extends AbstractParserLexer implements MailboxListParserInterface
{
	
	public const L_LABRA = 1; // <
	public const L_RABRA = 2; // >
	public const L_DQUOT = 3; // "
	public const L_ASLSH = 4; // \
	public const L_COMMA = 5; // ,
	
	public const T_ANGAD_PART = 10; // <abc@de
	public const T_ANGAD_END = 11;  // <abd@de>
	
	public const T_QUSTR_PART = 12; // "abc
	public const T_QUSTR_END = 13;  // "abc"
	public const T_QUSTR_SLSH = 14; // "abc\
	public const T_UNQTD_PART = 15; // abc OR ab"abc"de
	
	/**
	 * The mailbox parser.
	 * 
	 * @var ?MailboxParserInterface
	 */
	protected $_mailboxParser;
	
	/**
	 * Builds a new MailboxListParser on top of a mailbox parser if given.
	 * 
	 * @param MailboxParserInterface $mailboxParser
	 */
	public function __construct(?MailboxParserInterface $mailboxParser = null)
	{
		parent::__construct(MailboxListInterface::class);
		$this->_mailboxParser = $mailboxParser;
		
		$this->_config->addMappings('<', self::L_LABRA);
		$this->_config->addMappings('>', self::L_RABRA);
		$this->_config->addMappings('"', self::L_DQUOT);
		$this->_config->addMappings('\\', self::L_ASLSH);
		$this->_config->addMappings(',', self::L_COMMA);
		
		$this->_config->addMerging(LexerInterface::L_TRASH, LexerInterface::L_TRASH, self::T_UNQTD_PART);
		$this->_config->addMerging(self::T_UNQTD_PART, self::L_ASLSH, self::T_UNQTD_PART);
		$this->_config->addMerging(self::T_UNQTD_PART, self::L_DQUOT, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_UNQTD_PART, LexerInterface::L_TRASH, self::T_UNQTD_PART);
		
		$this->_config->addMerging(LexerInterface::L_TRASH, self::L_DQUOT, self::T_QUSTR_PART);
		$this->_config->addMerging(self::L_DQUOT, LexerInterface::L_TRASH, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_PART, LexerInterface::L_TRASH, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_PART, self::L_LABRA, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_PART, self::L_RABRA, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_PART, self::L_DQUOT, self::T_QUSTR_END);
		$this->_config->addMerging(self::T_QUSTR_PART, self::L_ASLSH, self::T_QUSTR_SLSH);
		$this->_config->addMerging(self::T_QUSTR_PART, self::L_COMMA, self::T_QUSTR_PART);
		
		$this->_config->addMerging(self::T_QUSTR_SLSH, LexerInterface::L_TRASH, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_SLSH, self::L_LABRA, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_SLSH, self::L_RABRA, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_SLSH, self::L_DQUOT, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_SLSH, self::L_ASLSH, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_SLSH, self::L_COMMA, self::T_QUSTR_PART);
		
		$this->_config->addMerging(self::T_QUSTR_END, LexerInterface::L_TRASH, self::T_UNQTD_PART);
		
		$this->_config->addMerging(self::L_LABRA, LexerInterface::L_TRASH, self::T_ANGAD_PART);
		$this->_config->addMerging(self::T_ANGAD_PART, LexerInterface::L_TRASH, self::T_ANGAD_PART);
		$this->_config->addMerging(self::T_ANGAD_PART, self::L_LABRA, self::T_ANGAD_PART);
		$this->_config->addMerging(self::T_ANGAD_PART, self::L_RABRA, self::T_ANGAD_END);
		$this->_config->addMerging(self::T_ANGAD_PART, self::L_DQUOT, self::T_ANGAD_PART);
		$this->_config->addMerging(self::T_ANGAD_PART, self::L_ASLSH, self::T_ANGAD_PART);
		$this->_config->addMerging(self::T_ANGAD_PART, self::L_COMMA, self::T_ANGAD_PART);
	}
	
	/**
	 * Gets the mailbox address parser.
	 * 
	 * @return MailboxParserInterface
	 */
	public function getMailboxParser() : MailboxParserInterface
	{
		if(null === $this->_mailboxParser)
		{
			$this->_mailboxParser = new MailboxParser();
		}
		
		return $this->_mailboxParser;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressParserInterface::parseString()
	 */
	public function parse(?string $data) : MailboxListInterface
	{
		return parent::parse($data); // interface compatibility
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxParserInterface::parseString()
	 */
	public function parseLexer(LexerInterface $lexer) : MailboxListInterface
	{
		$lexer->rewind();
		$mailboxes = [];
		$token = null;
		$data = '';
		
		while($lexer->valid())
		{
			$token = $this->expectOneOf($lexer, [
				LexerInterface::L_TRASH,
				self::T_ANGAD_PART,
				self::T_ANGAD_END,
				self::T_QUSTR_PART,
				self::T_QUSTR_END,
				self::T_QUSTR_SLSH,
				self::T_UNQTD_PART,
				self::L_COMMA,
			], $token);
			
			if($token->getCode() === self::L_COMMA)
			{
				$mailboxes[] = $this->getMailboxParser()->parse($data);
				$data = '';
				continue;
			}
			
			$data .= $token->getData();
		}
		
		if('' !== $data)
		{
			$mailboxes[] = $this->getMailboxParser()->parse($data);
		}
		
		return new MailboxList($mailboxes);
	}
	
}
