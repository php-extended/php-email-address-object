<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use InvalidArgumentException;
use LengthException;
use PhpExtended\Domain\Domain;
use PhpExtended\Ip\Ipv4AddressParser;
use PhpExtended\Ip\Ipv4AddressParserInterface;
use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;
use PhpExtended\Parser\ParseException;
use PhpExtended\Parser\ParseThrowable;

/**
 * EmailAddressParser class file.
 * 
 * This class is a simple implementation of the EmailAddressParserInterface.
 * 
 * The full state machine with ip addresses and escapable characters is not
 * implemented. A full representation of the state machine can be found on
 * regexper.com. Assumptions are reduced to the minimum regarding the email
 * addresses that can be validated with this "parser", remember to send the
 * email to have a true validation that the email exists and that the client
 * is not fake.
 * 
 * @see https://i.stack.imgur.com/YI6KR.png
 * @see https://regexper.com/#(%3F%3A%5Ba-z0-9!%23%24%25%26'*%2B%2F%3D%3F%5E_%60%7B%7C%7D~-%5D%2B(%3F%3A%5C.%5Ba-z0-9!%23%24%25%26'*%2B%2F%3D%3F%5E_%60%7B%7C%7D~-%5D%2B)*%7C%22(%3F%3A%5B%5Cx01-%5Cx08%5Cx0b%5Cx0c%5Cx0e-%5Cx1f%5Cx21%5Cx23-%5Cx5b%5Cx5d-%5Cx7f%5D%7C%5C%5C%5B%5Cx01-%5Cx09%5Cx0b%5Cx0c%5Cx0e-%5Cx7f%5D)*%22)%40(%3F%3A(%3F%3A%5Ba-z0-9%5D(%3F%3A%5Ba-z0-9-%5D*%5Ba-z0-9%5D)%3F%5C.)%2B%5Ba-z0-9%5D(%3F%3A%5Ba-z0-9-%5D*%5Ba-z0-9%5D)%3F%7C%5C%5B(%3F%3A(%3F%3A(2(5%5B0-5%5D%7C%5B0-4%5D%5B0-9%5D)%7C1%5B0-9%5D%5B0-9%5D%7C%5B1-9%5D%3F%5B0-9%5D))%5C.)%7B3%7D(%3F%3A(2(5%5B0-5%5D%7C%5B0-4%5D%5B0-9%5D)%7C1%5B0-9%5D%5B0-9%5D%7C%5B1-9%5D%3F%5B0-9%5D)%7C%5Ba-z0-9-%5D*%5Ba-z0-9%5D%3A(%3F%3A%5B%5Cx01-%5Cx08%5Cx0b%5Cx0c%5Cx0e-%5Cx1f%5Cx21-%5Cx5a%5Cx53-%5Cx7f%5D%7C%5C%5C%5B%5Cx01-%5Cx09%5Cx0b%5Cx0c%5Cx0e-%5Cx7f%5D)%2B)%5C%5D)
 * @see https://stackoverflow.com/questions/201323/how-to-validate-an-email-address-using-a-regular-expression
 * @see https://stackoverflow.com/questions/48055431/can-it-cause-harm-to-validate-email-addresses-with-a-regex
 * @see https://tools.ietf.org/html/rfc5322
 * @see https://www.linuxjournal.com/article/9585
 * 
 * @author Anastaszor
 * @extends AbstractParserLexer<EmailAddressInterface>
 */
class EmailAddressParser extends AbstractParserLexer implements EmailAddressParserInterface
{
	
	// missing from the parsing list (= trash) :
	// \x00 (nul)
	// \x0a (lf)
	// \x0d (cr)
	
	public const L_DIGITS = 1;     // [0-9]
	public const L_ALPHAS = 2;     // [A-Za-z]
	public const L_DOT = 3;        // .
	public const L_OPEN_PAREN = 4; // (
	public const L_CLOS_PAREN = 5; // )
	public const L_OPEN_SQBRA = 6; // [
	public const L_CLOS_SQBRA = 7; // ]
	public const L_AROBASE = 8;    // @
	public const L_ANTISLASH = 9;  // \
	public const L_DBL_QUOTE = 10; // "
	public const L_DASH = 11;      // -
	public const L_SYMBOL = 12;    // !#$%&'*+/=?^_`{|}~
	public const L_PRINTABLE = 13; // ,:;<>
	public const L_ENTITY = 14;
	//     \x01\x02\x03\x04\x05\x06\x07\x08\x09    \x0b\x0c    \x0e\x0f
	// \x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f
	// \x20                                                        \x7f
	
	public const T_LOCAL_PART = 100;  // a-z0-9+symbol+dash *repeat
	public const T_LOCAL_END = 101;   // local part that ended
	public const T_LOCAL_ESC = 102;   // abc\
	public const T_LOCAL_DOT = 103;   // abc.
	
	public const T_QUOTED_PART = 110; // "abc
	public const T_QUOTED_ESC = 111;  // "abc\
	
	// T_DOMAIN_* and T_IP_* data starts with an @
	public const T_DOMAIN_PART = 140; // abc
	public const T_DOMAIN_DOT = 141;  // abc.
	public const T_DOMAIN_DASH = 142; // abc-
	
	public const T_IP_START = 150;    // [
	public const T_IP_FIRST = 151;    // [12
	public const T_IP_FDOT = 152;     // [12.
	public const T_IP_SECOND = 153;   // [12.14
	public const T_IP_SDOT = 154;     // [12.14.
	public const T_IP_THIRD = 155;    // [12.14.16
	public const T_IP_TDOT = 156;     // [12.14.16.
	public const T_IP_FOURTH = 157;   // [12.14.16.18
	public const T_IP_END = 158;      // [12.14.16.18]
	
	
	/**
	 * The ipv4 parser.
	 * 
	 * @var ?Ipv4AddressParserInterface
	 */
	protected ?Ipv4AddressParserInterface $_ipv4Parser = null;
	
	/**
	 * Builds a new MailboxGroupListParser with its configuration.
	 * 
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function __construct()
	{
		parent::__construct(EmailAddressInterface::class);
		$this->_config->addMappings(LexerInterface::CLASS_DIGIT, self::L_DIGITS);
		$this->_config->addMappings(LexerInterface::CLASS_ALPHA, self::L_ALPHAS);
		$this->_config->addMappings('.', self::L_DOT);
		$this->_config->addMappings('(', self::L_OPEN_PAREN);
		$this->_config->addMappings(')', self::L_CLOS_PAREN);
		$this->_config->addMappings('[', self::L_OPEN_SQBRA);
		$this->_config->addMappings(']', self::L_CLOS_SQBRA);
		$this->_config->addMappings('@', self::L_AROBASE);
		$this->_config->addMappings('\\', self::L_ANTISLASH);
		$this->_config->addMappings('"', self::L_DBL_QUOTE);
		$this->_config->addMappings('-', self::L_DASH);
		$this->_config->addMappings('!#$%&\'*+/=?^_`{|}~', self::L_SYMBOL);
		$this->_config->addMappings(',:;<>', self::L_PRINTABLE);
		$this->_config->addMappings("\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0b\x0c\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f\x20\x7f", self::L_ENTITY);
		
		$this->_config->addMerging(self::L_DIGITS, self::L_DIGITS, self::L_DIGITS);
		$this->_config->addMerging(self::L_ALPHAS, self::L_ALPHAS, self::L_ALPHAS);
		
		$this->_config->addMerging(self::L_DBL_QUOTE, self::L_DIGITS, self::T_QUOTED_PART);
		$this->_config->addMerging(self::L_DBL_QUOTE, self::L_ALPHAS, self::T_QUOTED_PART);
		$this->_config->addMerging(self::L_DBL_QUOTE, self::L_DOT, self::T_QUOTED_PART);
		$this->_config->addMerging(self::L_DBL_QUOTE, self::L_OPEN_PAREN, self::T_QUOTED_PART);
		$this->_config->addMerging(self::L_DBL_QUOTE, self::L_CLOS_PAREN, self::T_QUOTED_PART);
		$this->_config->addMerging(self::L_DBL_QUOTE, self::L_OPEN_SQBRA, self::T_QUOTED_PART);
		$this->_config->addMerging(self::L_DBL_QUOTE, self::L_CLOS_SQBRA, self::T_QUOTED_PART);
		$this->_config->addMerging(self::L_DBL_QUOTE, self::L_AROBASE, self::T_QUOTED_PART);
		$this->_config->addMerging(self::L_DBL_QUOTE, self::L_ANTISLASH, self::T_QUOTED_ESC);
		$this->_config->addMerging(self::L_DBL_QUOTE, self::L_DASH, self::T_QUOTED_PART);
		$this->_config->addMerging(self::L_DBL_QUOTE, self::L_SYMBOL, self::T_QUOTED_PART);
		$this->_config->addMerging(self::L_DBL_QUOTE, self::L_PRINTABLE, self::T_QUOTED_PART);
		$this->_config->addMerging(self::L_DBL_QUOTE, self::L_ENTITY, self::T_QUOTED_PART);
		$this->_config->addMerging(self::L_DBL_QUOTE, self::L_DBL_QUOTE, self::T_LOCAL_END);
		
		$this->_config->addMerging(self::T_QUOTED_PART, self::L_DIGITS, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_PART, self::L_ALPHAS, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_PART, self::L_DOT, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_PART, self::L_OPEN_PAREN, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_PART, self::L_CLOS_PAREN, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_PART, self::L_OPEN_SQBRA, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_PART, self::L_CLOS_SQBRA, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_PART, self::L_AROBASE, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_PART, self::L_ANTISLASH, self::T_QUOTED_ESC);
		$this->_config->addMerging(self::T_QUOTED_PART, self::L_DASH, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_PART, self::L_SYMBOL, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_PART, self::L_PRINTABLE, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_PART, self::L_ENTITY, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_PART, self::L_DBL_QUOTE, self::T_LOCAL_END);
		
		$this->_config->addMerging(self::T_QUOTED_ESC, self::L_DIGITS, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_ESC, self::L_ALPHAS, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_ESC, self::L_DOT, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_ESC, self::L_OPEN_PAREN, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_ESC, self::L_CLOS_PAREN, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_ESC, self::L_OPEN_SQBRA, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_ESC, self::L_CLOS_SQBRA, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_ESC, self::L_AROBASE, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_ESC, self::L_ANTISLASH, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_ESC, self::L_DASH, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_ESC, self::L_SYMBOL, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_ESC, self::L_PRINTABLE, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_ESC, self::L_ENTITY, self::T_QUOTED_PART);
		$this->_config->addMerging(self::T_QUOTED_ESC, self::L_DBL_QUOTE, self::T_QUOTED_PART);
		
		$this->_config->addMerging(self::L_ALPHAS, self::L_SYMBOL, self::T_LOCAL_PART);
		$this->_config->addMerging(self::L_DIGITS, self::L_SYMBOL, self::T_LOCAL_PART);
		$this->_config->addMerging(self::L_SYMBOL, self::L_SYMBOL, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_PART, self::L_ALPHAS, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_PART, self::L_DIGITS, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_PART, self::L_SYMBOL, self::T_LOCAL_PART);
		$this->_config->addMerging(self::L_ALPHAS, self::L_ANTISLASH, self::T_LOCAL_ESC);
		$this->_config->addMerging(self::L_DIGITS, self::L_ANTISLASH, self::T_LOCAL_ESC);
		$this->_config->addMerging(self::L_SYMBOL, self::L_ANTISLASH, self::T_LOCAL_ESC);
		$this->_config->addMerging(self::T_LOCAL_PART, self::L_ANTISLASH, self::T_LOCAL_ESC);
		$this->_config->addMerging(self::T_LOCAL_PART, self::L_DOT, self::T_LOCAL_DOT);
		
		$this->_config->addMerging(self::T_LOCAL_ESC, self::L_DIGITS, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_ESC, self::L_ALPHAS, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_ESC, self::L_DOT, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_ESC, self::L_OPEN_PAREN, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_ESC, self::L_CLOS_PAREN, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_ESC, self::L_OPEN_SQBRA, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_ESC, self::L_CLOS_SQBRA, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_ESC, self::L_AROBASE, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_ESC, self::L_ANTISLASH, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_ESC, self::L_DASH, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_ESC, self::L_SYMBOL, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_ESC, self::L_PRINTABLE, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_ESC, self::L_ENTITY, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_ESC, self::L_DBL_QUOTE, self::T_LOCAL_PART);
		
		$this->_config->addMerging(self::T_LOCAL_DOT, self::L_DIGITS, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_DOT, self::L_ALPHAS, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_DOT, self::L_ANTISLASH, self::T_LOCAL_ESC);
		$this->_config->addMerging(self::T_LOCAL_DOT, self::L_DASH, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_DOT, self::L_SYMBOL, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_LOCAL_DOT, self::L_DBL_QUOTE, self::T_QUOTED_PART);
		
		$this->_config->addMerging(self::T_LOCAL_END, self::L_DOT, self::T_LOCAL_DOT);
		
		$this->_config->addMerging(self::L_DIGITS, self::L_ALPHAS, self::T_DOMAIN_PART);
		$this->_config->addMerging(self::L_DIGITS, self::L_DOT, self::T_DOMAIN_DOT);
		$this->_config->addMerging(self::L_DIGITS, self::L_ANTISLASH, self::T_LOCAL_ESC);
		$this->_config->addMerging(self::L_DIGITS, self::L_DASH, self::T_DOMAIN_DASH);
		$this->_config->addMerging(self::L_DIGITS, self::L_SYMBOL, self::T_LOCAL_PART);
		
		$this->_config->addMerging(self::L_ALPHAS, self::L_DIGITS, self::T_DOMAIN_PART);
		$this->_config->addMerging(self::L_ALPHAS, self::L_DOT, self::T_DOMAIN_DOT);
		$this->_config->addMerging(self::L_ALPHAS, self::L_ANTISLASH, self::T_LOCAL_ESC);
		$this->_config->addMerging(self::L_ALPHAS, self::L_DASH, self::T_DOMAIN_DASH);
		$this->_config->addMerging(self::L_ALPHAS, self::L_SYMBOL, self::T_LOCAL_PART);
		
		$this->_config->addMerging(self::T_DOMAIN_PART, self::L_DIGITS, self::T_DOMAIN_PART);
		$this->_config->addMerging(self::T_DOMAIN_PART, self::L_ALPHAS, self::T_DOMAIN_PART);
		$this->_config->addMerging(self::T_DOMAIN_PART, self::L_DOT, self::T_DOMAIN_DOT);
		$this->_config->addMerging(self::T_DOMAIN_PART, self::L_DASH, self::T_DOMAIN_DASH);
		$this->_config->addMerging(self::T_DOMAIN_DOT, self::L_DIGITS, self::T_DOMAIN_PART);
		$this->_config->addMerging(self::T_DOMAIN_DOT, self::L_ALPHAS, self::T_DOMAIN_PART);
		$this->_config->addMerging(self::T_DOMAIN_DASH, self::L_ALPHAS, self::T_DOMAIN_PART);
		$this->_config->addMerging(self::T_DOMAIN_DASH, self::L_DIGITS, self::T_DOMAIN_PART);
		$this->_config->addMerging(self::T_DOMAIN_DASH, self::L_DASH, self::T_DOMAIN_DASH);
		$this->_config->addMerging(self::T_DOMAIN_PART, self::L_SYMBOL, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_DOMAIN_DOT, self::L_SYMBOL, self::T_LOCAL_PART);
		$this->_config->addMerging(self::T_DOMAIN_DOT, self::L_DBL_QUOTE, self::T_QUOTED_PART);
		
		$this->_config->addMerging(self::L_OPEN_SQBRA, self::L_DIGITS, self::T_IP_FIRST);
		$this->_config->addMerging(self::T_IP_FIRST, self::L_DIGITS, self::T_IP_FIRST);
		$this->_config->addMerging(self::T_IP_FIRST, self::L_DOT, self::T_IP_FDOT);
		$this->_config->addMerging(self::T_IP_FDOT, self::L_DIGITS, self::T_IP_SECOND);
		$this->_config->addMerging(self::T_IP_SECOND, self::L_DIGITS, self::T_IP_SECOND);
		$this->_config->addMerging(self::T_IP_SECOND, self::L_DOT, self::T_IP_SDOT);
		$this->_config->addMerging(self::T_IP_SDOT, self::L_DIGITS, self::T_IP_THIRD);
		$this->_config->addMerging(self::T_IP_THIRD, self::L_DIGITS, self::T_IP_THIRD);
		$this->_config->addMerging(self::T_IP_THIRD, self::L_DOT, self::T_IP_TDOT);
		$this->_config->addMerging(self::T_IP_TDOT, self::L_DIGITS, self::T_IP_FOURTH);
		$this->_config->addMerging(self::T_IP_FOURTH, self::L_DIGITS, self::T_IP_FOURTH);
		$this->_config->addMerging(self::T_IP_FOURTH, self::L_CLOS_SQBRA, self::T_IP_END);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressParserInterface::parseString()
	 */
	public function parse(?string $data) : EmailAddressInterface
	{
		return parent::parse($data); // interface compatibility
	}
	
	/**
	 * Parses effectively the data as email data.
	 * 
	 * @param LexerInterface $lexer
	 * @return EmailAddressInterface
	 * @throws ParseThrowable
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function parseLexer(LexerInterface $lexer) : EmailAddressInterface
	{
		$lexer->rewind();
		$token = null;
		
		$token = $this->expectOneOf($lexer, [
			self::L_DIGITS,
			self::L_ALPHAS,
			self::L_OPEN_PAREN,
			self::L_CLOS_PAREN,
			self::L_OPEN_SQBRA,
			self::L_CLOS_SQBRA,
			self::L_ANTISLASH,
			self::L_DASH,
			self::L_SYMBOL,
			self::L_PRINTABLE,
			self::L_ENTITY,
			self::T_LOCAL_PART,
			self::T_LOCAL_END,
			self::T_DOMAIN_PART,
		], $token);
		$localPart = $token->getData();
		
		if('' === $localPart)
		{
			// should not happen
			// @codeCoverageIgnoreStart
			$message = 'Failed to find email address local part in given data.';
			
			throw new ParseException($this->_class, $token->getData(), $token->getColumn(), $message);
			// @codeCoverageIgnoreEnd
		}
		
		$token = $this->expectOneOf($lexer, [self::L_AROBASE], $token);
		
		$token = $this->expectOneOf($lexer, [self::L_ALPHAS, self::T_DOMAIN_PART, self::T_IP_FOURTH, self::T_IP_END], $token);
		
		$domain = null;
		$ipv4 = null;
		$ipv6 = null;
		
		switch($token->getCode())
		{
			case self::T_IP_END:
			case self::T_IP_FOURTH:
				$domainData = $token->getData();
				// remove the brackets, only keep the digits and dots
				$domainData = \str_replace('[', '', $domainData);
				$domainData = \str_replace(']', '', $domainData);
				
				try
				{
					$ipv4 = $this->getIpv4AddressParser()->parse($domainData);
				}
				catch(ParseException $exc)
				{
					$message = 'Failed to parse domain part as ipv4 address.';
					
					throw new ParseException($this->_class, $token->getData(), $token->getColumn(), $message, -1, $exc);
				}
				break;
				
			default:
				$domainData = $token->getData();
				if('' === $domainData)
				{
					// should not happen
					// @codeCoverageIgnoreStart
					$message = 'Failed to find email address domain in given data, but found local part {local}';
					$context = ['{local}' => $localPart];
					
					throw new ParseException($this->_class, $token->getData(), $token->getColumn(), \strtr($message, $context));
					// @codeCoverageIgnoreEnd
				}
				
				try
				{
					$domain = new Domain(\explode('.', $domainData));
				}
				catch(InvalidArgumentException|LengthException $exc)
				{
					$message = 'Failed to parse domain part as domain address.';
					
					throw new ParseException($this->_class, $token->getData(), $token->getColumn(), $message, -1, $exc);
				}
				break;
		}
		
		$this->expectEof($lexer, $token);
		
		try
		{
			return new EmailAddress($localPart, $domain, $ipv4, $ipv6);
		}
		catch(InvalidArgumentException $exc)
		{
			$message = 'Failed to create new email address with {local} @ {domain}';
			$context = ['{local}' => $localPart, '{domain}' => $token->getData()];
			
			throw new ParseException($this->_class, $localPart.'@'.$token->getData(), 0, \strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * Gets the ipv4 address parser.
	 * 
	 * @return Ipv4AddressParserInterface
	 */
	protected function getIpv4AddressParser() : Ipv4AddressParserInterface
	{
		if(null === $this->_ipv4Parser)
		{
			$this->_ipv4Parser = new Ipv4AddressParser();
		}
		
		return $this->_ipv4Parser;
	}
	
}
