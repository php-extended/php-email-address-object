<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use InvalidArgumentException;
use LengthException;
use PhpExtended\Domain\Domain;
use PhpExtended\Domain\DomainInterface;
use PhpExtended\Ip\Ipv4AddressInterface;
use PhpExtended\Ip\Ipv6AddressInterface;

/**
 * EmailAddress class file.
 * 
 * This class represents a single email address.
 * 
 * The local ip addresses of the domain part (RFC 5322) is not supported. No
 * additional checks are performed regarding the domain part to check its
 * validity.
 * 
 * @author Anastaszor
 */
class EmailAddress implements EmailAddressInterface
{
	
	/**
	 * The local value of the email address.
	 * 
	 * @var string
	 */
	protected string $_local;
	
	/**
	 * The domain of the email address.
	 * 
	 * @var ?DomainInterface
	 */
	protected ?DomainInterface $_domain = null;
	
	/**
	 * The domain-ipv4 of the email address.
	 * 
	 * @var ?Ipv4AddressInterface
	 */
	protected ?Ipv4AddressInterface $_ipv4 = null;
	
	/**
	 * The domain-ipv6 of the email address.
	 * 
	 * @var ?Ipv6AddressInterface
	 */
	protected ?Ipv6AddressInterface $_ipv6 = null;
	
	/**
	 * Builds a new EmailAddress with the given email address, or with the
	 * local part and the domain part, if both are given. If the domain is
	 * given, then there is no need for the full email on the first argument,
	 * only the local part is needed. If both are given and the email is
	 * complete, then the given domain will overwrite the domain of the given
	 * email.
	 * 
	 * @param string $local
	 * @param ?DomainInterface $domain
	 * @param ?Ipv4AddressInterface $ipv4
	 * @param ?Ipv6AddressInterface $ipv6
	 * @throws InvalidArgumentException if multiple domain/ipv4/ipv6 provided
	 * @throws InvalidArgumentException if no domain no ipv4 no ipv6 provided
	 * @throws InvalidArgumentException if the localpart is too long
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function __construct(string $local, ?DomainInterface $domain, ?Ipv4AddressInterface $ipv4 = null, ?Ipv6AddressInterface $ipv6 = null)
	{
		if(null === $domain && null === $ipv4 && null === $ipv6)
		{
			throw new InvalidArgumentException('All of the domain, ipv4 and ipv6 are null. Exactly one must be not null.');
		}
		
		$domainIpv4NotNull = null !== $domain && null !== $ipv4;
		$domainIpv6NotNull = null !== $domain && null !== $ipv6;
		$ipv4ipv6NotNull = null !== $ipv4 && null !== $ipv6;
		if($domainIpv4NotNull || $domainIpv6NotNull || $ipv4ipv6NotNull)
		{
			throw new InvalidArgumentException('Both of the '.($domainIpv4NotNull ? 'domain and ipv4 ' : '').($domainIpv6NotNull ? 'domain and ipv6 ' : '').($ipv4ipv6NotNull ? 'ipv4 and ipv6 ' : '').'are not null. Exactly one must be not null.');
		}
		
		$this->_domain = $domain;
		$this->_ipv4 = $ipv4;
		$this->_ipv6 = $ipv6;
		$this->_local = $this->checkLocalPart($local);
		$this->checkLength();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getCanonicalRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressInterface::getLocalPart()
	 */
	public function getLocalPart() : string
	{
		return $this->_local;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressInterface::withLocalPart()
	 * @throws InvalidArgumentException
	 */
	public function withLocalPart(string $local) : EmailAddressInterface
	{
		return new EmailAddress($local, $this->_domain, $this->_ipv4, $this->_ipv6);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressInterface::hasIpAsDomain()
	 */
	public function hasIpAsDomain() : bool
	{
		return null !== $this->_ipv4 || null !== $this->_ipv6;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressInterface::getDomain()
	 * @throws InvalidArgumentException
	 * @throws LengthException
	 */
	public function getDomain() : DomainInterface
	{
		if(null !== $this->_domain)
		{
			return $this->_domain;
		}
		
		if(null !== $this->_ipv4)
		{
			return new Domain([
				(string) $this->_ipv4->getFourthByte(),
				(string) $this->_ipv4->getThirdByte(),
				(string) $this->_ipv4->getSecondByte(),
				(string) $this->_ipv4->getFirstByte(),
				'in-addr',
				'arpa',
			]);
		}
		
		if(null !== $this->_ipv6)
		{
			$ip6str = $this->_ipv6->getCanonicalRepresentation();
			$ip6str = \str_replace(':', '', $ip6str);
			$ip6str = \strrev($ip6str);
			
			return new Domain(\array_merge((array) \str_split($ip6str, 1), [
				'ip6',
				'arpa',
			]));
		}
		
		// should not happen
		// @codeCoverageIgnoreStart
		return new Domain(['example', 'com']);
		// @codeCoverageIgnoreEnd
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressInterface::withDomain()
	 * @throws InvalidArgumentException
	 */
	public function withDomain(DomainInterface $domain) : EmailAddressInterface
	{
		return new EmailAddress($this->_local, $domain, null, null);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressInterface::getIpv4Address()
	 */
	public function getIpv4Address() : ?Ipv4AddressInterface
	{
		return $this->_ipv4;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressInterface::withIpv4Address()
	 * @throws InvalidArgumentException
	 */
	public function withIpv4Address(Ipv4AddressInterface $address) : EmailAddressInterface
	{
		return new EmailAddress($this->_local, null, $address, null);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressInterface::getIpv6Address()
	 */
	public function getIpv6Address() : ?Ipv6AddressInterface
	{
		return $this->_ipv6;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressInterface::withIpv6Address()
	 * @throws InvalidArgumentException
	 */
	public function withIpv6Address(Ipv6AddressInterface $address) : EmailAddressInterface
	{
		return new EmailAddress($this->_local, null, null, $address);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressInterface::getCanonicalRepresentation()
	 */
	public function getCanonicalRepresentation() : string
	{
		$str = $this->quote($this->_local).'@';
		
		if(null !== $this->_domain)
		{
			return $str.$this->_domain->getCanonicalRepresentation();
		}
		
		if(null !== $this->_ipv4)
		{
			return $str.'['.$this->_ipv4->getCanonicalRepresentation().']';
		}
		
		if(null !== $this->_ipv6)
		{
			return $str.'[IPV6:'.$this->_ipv6->getCanonicalRepresentation().']';
		}
		
		// should not happen
		// @codeCoverageIgnoreStart
		return $str.'example.com';
		// @codeCoverageIgnoreEnd
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressInterface::equals()
	 */
	public function equals($other) : bool
	{
		return $other instanceof EmailAddressInterface
			&& $this->getCanonicalRepresentation() === $other->getCanonicalRepresentation();
	}
	
	/**
	 * Checks if the local part is valid, and returns a valid canonical local
	 * part.
	 * 
	 * @param string $local
	 * @return string
	 * @throws InvalidArgumentException
	 */
	protected function checkLocalPart(string $local) : string
	{
		$local = (string) \mb_strtolower($local);
		$parts = [];
		
		$ash = '\\';
		$replacementArray = [
			$ash.'"' => '"',   // transform escaped quotes in simple quotes
			$ash.$ash => $ash, // transform escaped slashes in simple slashes
			'"' => '',         // remove other quotes
			$ash => '',        // remove other slashes
		];
		
		foreach(\explode('.', $local) as $part)
		{
			$part = \strtr(\trim($part), $replacementArray);
			if(\mb_strlen($part) > 0)
			{
				$parts[] = $part;
			}
		}
		
		if(1 > \count($parts))
		{
			throw new InvalidArgumentException('The local part should not be empty.');
		}
		
		$cleaned = \implode('.', $parts);
		$quoted = $this->quote($cleaned);
		
		if(64 < \mb_strlen($quoted))
		{
			$message = 'The local part should be less than or equal to 64 characters with extra quotes, {k} provided in {data}';
			$context = ['{k}' => \mb_strlen($quoted), '{data}' => $quoted];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		return $cleaned;
	}
	
	/**
	 * Checks the effective total length of an email with the given local part
	 * and given domain.
	 * 
	 * @throws InvalidArgumentException if the length is too much
	 */
	protected function checkLength() : void
	{
		$forged = $this->getCanonicalRepresentation();
		if(\mb_strlen($forged) > 255)
		{
			$message = 'The total length of the email address should be less than or equal to 255 characters, {k} provided in {data}';
			$context = ['{k}' => \mb_strlen($forged), '{data}' => $forged];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
	}
	
	/**
	 * Gets the version of the local part as displayed to a mail server.
	 * 
	 * @param string $local
	 * @return string
	 */
	protected function quote(string $local) : string
	{
		if(!\preg_match('/^[A-Za-z0-9!#$%&\'*+\\/=?^_`{|}~.-]+$/', $local))
		{
			$local = \strtr($local, ['\\' => '\\\\']);
			$local = \strtr($local, ['"' => '\\"']);
			$local = '"'.$local.'"';
		}
		
		return $local;
	}
	
}
