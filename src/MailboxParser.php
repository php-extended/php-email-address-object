<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;

/**
 * MailboxParser class file.
 * 
 * This class is a simple implementation of the MailboxParserInterface.
 * 
 * @see https://www.rfc-editor.org/rfc/rfc2822#section-3.4
 * 
 * @author Anastaszor
 * @extends AbstractParserLexer<MailboxInterface>
 */
class MailboxParser extends AbstractParserLexer implements MailboxParserInterface
{
	
	public const L_LABRA = 1; // <
	public const L_RABRA = 2; // >
	public const L_DQUOT = 3; // "
	public const L_ASLSH = 4; // \
	
	public const T_ANGAD_PART = 10; // <abc@de
	public const T_ANGAD_END = 11;  // <abc@de>
	
	public const T_QUSTR_PART = 12; // "abc
	public const T_QUSTR_END = 13;  // "abc"
	public const T_QUSTR_SLSH = 14; // "abc\
	public const T_UNQTD_PART = 15; // abc OR ab"abc"de
	
	/**
	 * The email address parser.
	 * 
	 * @var ?EmailAddressParserInterface
	 */
	protected $_emailAddressParser;
	
	/**
	 * Builds a new MailboxParser on top of an email address parser if given.
	 * 
	 * @param EmailAddressParserInterface $emailAddressParser
	 */
	public function __construct(?EmailAddressParserInterface $emailAddressParser = null)
	{
		parent::__construct(MailboxInterface::class);
		$this->_emailAddressParser = $emailAddressParser;
		
		$this->_config->addMappings('<', self::L_LABRA);
		$this->_config->addMappings('>', self::L_RABRA);
		$this->_config->addMappings('"', self::L_DQUOT);
		$this->_config->addMappings('\\', self::L_ASLSH);
		
		$this->_config->addMerging(LexerInterface::L_TRASH, LexerInterface::L_TRASH, self::T_UNQTD_PART);
		$this->_config->addMerging(self::T_UNQTD_PART, self::L_ASLSH, self::T_UNQTD_PART);
		$this->_config->addMerging(self::T_UNQTD_PART, self::L_DQUOT, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_UNQTD_PART, LexerInterface::L_TRASH, self::T_UNQTD_PART);
		
		$this->_config->addMerging(LexerInterface::L_TRASH, self::L_DQUOT, self::T_QUSTR_PART);
		$this->_config->addMerging(self::L_DQUOT, LexerInterface::L_TRASH, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_PART, LexerInterface::L_TRASH, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_PART, self::L_LABRA, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_PART, self::L_RABRA, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_PART, self::L_DQUOT, self::T_QUSTR_END);
		$this->_config->addMerging(self::T_QUSTR_PART, self::L_ASLSH, self::T_QUSTR_SLSH);
		
		$this->_config->addMerging(self::T_QUSTR_SLSH, LexerInterface::L_TRASH, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_SLSH, self::L_LABRA, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_SLSH, self::L_RABRA, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_SLSH, self::L_DQUOT, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_SLSH, self::L_ASLSH, self::T_QUSTR_PART);
		
		$this->_config->addMerging(self::T_QUSTR_END, LexerInterface::L_TRASH, self::T_UNQTD_PART);
		
		$this->_config->addMerging(self::L_LABRA, LexerInterface::L_TRASH, self::T_ANGAD_PART);
		$this->_config->addMerging(self::T_ANGAD_PART, LexerInterface::L_TRASH, self::T_ANGAD_PART);
		$this->_config->addMerging(self::T_ANGAD_PART, self::L_LABRA, self::T_ANGAD_PART);
		$this->_config->addMerging(self::T_ANGAD_PART, self::L_RABRA, self::T_ANGAD_END);
		$this->_config->addMerging(self::T_ANGAD_PART, self::L_DQUOT, self::T_ANGAD_PART);
		$this->_config->addMerging(self::T_ANGAD_PART, self::L_ASLSH, self::T_ANGAD_PART);
	}
	
	/**
	 * Gets the email address parser.
	 * 
	 * @return EmailAddressParserInterface
	 */
	public function getEmailAddressParser() : EmailAddressParserInterface
	{
		if(null === $this->_emailAddressParser)
		{
			$this->_emailAddressParser = new EmailAddressParser();
		}
		
		return $this->_emailAddressParser;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressParserInterface::parseString()
	 */
	public function parse(?string $data) : MailboxInterface
	{
		return parent::parse($data); // interface compatibility
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxParserInterface::parseString()
	 */
	public function parseLexer(LexerInterface $lexer) : MailboxInterface
	{
		$lexer->rewind();
		$named = null;
		
		$token = $this->maybeOneOf($lexer, [
			LexerInterface::L_TRASH,
			self::T_QUSTR_PART,
			self::T_QUSTR_END,
			self::T_QUSTR_SLSH,
			self::T_UNQTD_PART,
		]);
		if(null !== $token)
		{
			$named = (string) \trim($token->getData());
			if('' !== $named && '"' === $named[0] && $named[(int) \strlen($named) - 1] === '"')
			{
				$named = (string) \substr($named, 1, -1);
			}
		}
		
		$token = $this->maybeOneOf($lexer, [
			self::T_ANGAD_PART,
			self::T_ANGAD_END,
		]);
		if(null !== $token)
		{
			$data = $token->getData();
			if('<' === $data[0])
			{
				$data = (string) \substr($data, 1);
			}
			if($data[(int) \strlen($data) - 1] === '>')
			{
				$data = (string) \substr($data, 0, -1);
			}
			
			return new Mailbox($this->getEmailAddressParser()->parse($data), $named);
		}
		
		// if we do not find any <>, this means that maybe what we took for
		// the name contains the email address, give it a try
		return new Mailbox($this->getEmailAddressParser()->parse($named));
	}
	
}
