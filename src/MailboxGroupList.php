<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use ArrayIterator;
use Iterator;
use PhpExtended\Domain\DomainInterface;

/**
 * MailboxGroupList class file.
 * 
 * This class is a simple implementation of the MailboxGroupListInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class MailboxGroupList implements MailboxGroupListInterface
{
	
	/**
	 * The store of mailbox groups.
	 * 
	 * @var array<integer, MailboxGroupInterface>
	 */
	protected array $_groups = [];
	
	/**
	 * Builds a new MailboxGroup List with the given array of mailbox groups.
	 * 
	 * @param array<integer, MailboxGroupInterface> $list
	 */
	public function __construct(array $list = [])
	{
		foreach($list as $group)
		{
			$this->addMailboxGroup($group);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getCanonicalRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_groups);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::current()
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function current() : MailboxGroupInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return \current($this->_groups);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		\next($this->_groups);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return (int) \key($this->_groups);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return false !== \current($this->_groups);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		\reset($this->_groups);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::withDomain()
	 */
	public function withDomain(DomainInterface $domain) : MailboxGroupListInterface
	{
		$list = [];
		
		foreach($this->_groups as $group)
		{
			$list[] = $group->withDomain($domain);
		}
		
		return new MailboxGroupList($list);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::filterByDomain()
	 */
	public function filterByDomain(DomainInterface $domain) : MailboxGroupListInterface
	{
		$list = [];
		
		foreach($this->_groups as $group)
		{
			$filtered = $group->filterByDomain($domain);
			if(!$filtered->isEmpty())
			{
				$list[] = $filtered;
			}
		}
		
		return new MailboxGroupList($list);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::withEmailAddress()
	 */
	public function withEmailAddress(EmailAddressInterface $email, ?string $display = null, ?string $group = null) : MailboxGroupListInterface
	{
		$mgl = new MailboxGroupList($this->_groups);
		$found = false;
		
		foreach($mgl as $k => $current)
		{
			if($current->getDisplayName() === (string) $group)
			{
				$mgl->_groups[$k] = $current->withEmailAddress($email, $display);
				$found = true;
				break;
			}
		}
		
		if(!$found)
		{
			$mgl->addMailboxGroup(new MailboxGroup(new MailboxList([new Mailbox($email, $display)]), $group));
		}
		
		return $mgl;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::withEmailAddressList()
	 */
	public function withEmailAddressList(EmailAddressListInterface $addressList, ?string $group = null) : MailboxGroupListInterface
	{
		$mgl = new MailboxGroupList($this->_groups);
		$found = false;
		
		foreach($mgl as $k => $current)
		{
			if($current->getDisplayName() === (string) $group)
			{
				$mgl->_groups[$k] = $current->withEmailAddressList($addressList);
				$found = true;
				break;
			}
		}
		
		if(!$found)
		{
			$mailboxgroup = new MailboxGroup(new MailboxList([]), $group);
			$mailboxgroup = $mailboxgroup->withEmailAddressList($addressList);
			$mgl->addMailboxGroup($mailboxgroup);
		}
		
		return $mgl;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::withMailbox()
	 */
	public function withMailbox(MailboxInterface $mailbox, ?string $group = null) : MailboxGroupListInterface
	{
		$mgl = new MailboxGroupList($this->_groups);
		$found = false;
		
		foreach($mgl as $k => $current)
		{
			if($current->getDisplayName() === (string) $group)
			{
				$mgl->_groups[$k] = $current->withMailbox($mailbox);
				$found = true;
				break;
			}
		}
		
		if(!$found)
		{
			$mgl->addMailboxGroup(new MailboxGroup(new MailboxList([$mailbox]), $group));
		}
		
		return $mgl;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::withMailboxList()
	 */
	public function withMailboxList(MailboxListInterface $mailboxList, ?string $group = null) : MailboxGroupListInterface
	{
		$mgl = new MailboxGroupList($this->_groups);
		$found = false;
		
		foreach($mgl as $k => $current)
		{
			if($current->getDisplayName() === (string) $group)
			{
				$mgl->_groups[$k] = $current->withMailboxList($mailboxList);
				$found = true;
				break;
			}
		}
		
		if(!$found)
		{
			$mgl->addMailboxGroup(new MailboxGroup($mailboxList, $group));
		}
		
		return $mgl;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::withMailboxGroup()
	 */
	public function withMailboxGroup(MailboxGroupInterface $group) : MailboxGroupListInterface
	{
		return (new MailboxGroupList($this->_groups))->addMailboxGroup($group);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::withMailboxGroupList()
	 */
	public function withMailboxGroupList(MailboxGroupListInterface $mailboxGroupList) : MailboxGroupListInterface
	{
		$mgl = new MailboxGroupList($this->_groups);
		
		foreach($mailboxGroupList as $group)
		{
			$mgl->addMailboxGroup($group);
		}
		
		return $mgl;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::getCanonicalRepresentation()
	 */
	public function getCanonicalRepresentation() : string
	{
		$str = [];
		
		foreach($this->_groups as $group)
		{
			$str[] = $group->getCanonicalRepresentation();
		}
		
		return \implode('; ', $str);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressListInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return empty($this->_groups);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::collectDisplayNames()
	 */
	public function collectDisplayNames() : Iterator
	{
		$names = [];
		
		foreach($this->_groups as $group)
		{
			foreach($group->collectDisplayNames() as $data)
			{
				$names[] = $data;
			}
		}
		
		return new ArrayIterator($names);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::collectDomains()
	 */
	public function collectDomains() : Iterator
	{
		$domains = [];
		
		foreach($this->_groups as $group)
		{
			/** @var DomainInterface $domain */
			foreach($group->collectDomains() as $domain)
			{
				$domains[$domain->getCanonicalRepresentation()] = $domain;
			}
		}
		
		return new ArrayIterator(\array_values($domains));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::collectEmailAddresses()
	 */
	public function collectEmailAddresses() : EmailAddressListInterface
	{
		$emails = [];
		
		foreach($this->_groups as $group)
		{
			foreach($group->collectEmailAddresses() as $email)
			{
				$emails[] = $email;
			}
		}
		
		return new EmailAddressList($emails);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::collectMailboxes()
	 */
	public function collectMailboxes() : MailboxListInterface
	{
		$mailboxes = [];
		
		foreach($this->_groups as $group)
		{
			foreach($group as $mailbox)
			{
				$mailboxes[] = $mailbox;
			}
		}
		
		return new MailboxList($mailboxes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::equals()
	 */
	public function equals($other) : bool
	{
		if(!$other instanceof MailboxGroupListInterface)
		{
			return false;
		}
		
		if($this->count() !== $other->count())
		{
			return false;
		}
		
		return $this->containsMailboxGroupList($other)
			&& $other->containsMailboxGroupList($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::containsEmailAddress()
	 */
	public function containsEmailAddress(EmailAddressInterface $address) : bool
	{
		foreach($this->_groups as $mailboxGroup)
		{
			if($mailboxGroup->containsEmailAddress($address))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::containsEmailAddressList()
	 */
	public function containsEmailAddressList(EmailAddressListInterface $addressList) : bool
	{
		foreach($addressList as $address)
		{
			if(!$this->containsEmailAddress($address))
			{
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::containsMailbox()
	 */
	public function containsMailbox(MailboxInterface $mailbox) : bool
	{
		foreach($this->_groups as $mailboxGroup)
		{
			if($mailboxGroup->containsMailbox($mailbox))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::containsMailboxIgnoreLabels()
	 */
	public function containsMailboxIgnoreLabels(MailboxInterface $mailbox) : bool
	{
		return $this->containsEmailAddress($mailbox->getEmailAddress());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::containsMailboxList()
	 */
	public function containsMailboxList(MailboxListInterface $mailboxList) : bool
	{
		foreach($this->_groups as $mailboxGroup)
		{
			if($mailboxGroup->containsMailboxList($mailboxList))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::containsMailboxListIgnoreLabels()
	 */
	public function containsMailboxListIgnoreLabels(MailboxListInterface $mailboxList) : bool
	{
		return $this->containsEmailAddressList($mailboxList->collectEmailAddresses());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::containsMailboxGroup()
	 */
	public function containsMailboxGroup(MailboxGroupInterface $mailboxGroup) : bool
	{
		foreach($this->_groups as $group)
		{
			if($group->equals($mailboxGroup))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::containsMailboxGroupIgnoreLabels()
	 */
	public function containsMailboxGroupIgnoreLabels(MailboxGroupInterface $mailboxGroup) : bool
	{
		return $this->containsEmailAddressList($mailboxGroup->collectEmailAddresses());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::containsMailboxGroupList()
	 */
	public function containsMailboxGroupList(MailboxGroupListInterface $mailboxGroupList) : bool
	{
		foreach($mailboxGroupList as $mailboxGroup)
		{
			if(!$this->containsMailboxGroup($mailboxGroup))
			{
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupListInterface::containsMailboxGroupListIgnoreLabels()
	 */
	public function containsMailboxGroupListIgnoreLabels(MailboxGroupListInterface $mailboxGroupList) : bool
	{
		return $this->containsEmailAddressList($mailboxGroupList->collectEmailAddresses());
	}
	
	/**
	 * Adds the given mailbox to this list.
	 * 
	 * @param MailboxGroupInterface $group
	 * @return MailboxGroupListInterface
	 */
	protected function addMailboxGroup(MailboxGroupInterface $group) : MailboxGroupListInterface
	{
		foreach($this->_groups as $k => $current)
		{
			if($current->getDisplayName() === $group->getDisplayName())
			{
				// merge the lists into the same group
				$this->_groups[$k] = $current->withMailboxList($group->getMailboxList());
				
				return $this;
			}
		}
		$this->_groups[] = $group;
		
		return $this;
	}
	
}
