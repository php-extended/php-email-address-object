<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use PhpExtended\Parser\AbstractParser;

/**
 * MailboxGroupParser class file.
 * 
 * This class is a simple implementation of the MailboxGroupParserInterface.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\AbstractParser<MailboxGroupInterface>
 */
class MailboxGroupParser extends AbstractParser implements MailboxGroupParserInterface
{
	
	/**
	 * The mailbox group list parser.
	 *
	 * @var ?MailboxGroupListParserInterface
	 */
	protected ?MailboxGroupListParserInterface $_mailboxGLParser = null;
	
	/**
	 * Gets the mailbox group list parser.
	 *
	 * @return MailboxGroupListParserInterface
	 */
	public function getMailboxGroupListParser() : MailboxGroupListParserInterface
	{
		if(null === $this->_mailboxGLParser)
		{
			$this->_mailboxGLParser = new MailboxGroupListParser();
		}
		
		return $this->_mailboxGLParser;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupParserInterface::parseString()
	 */
	public function parse(?string $data) : MailboxGroupInterface
	{
		$mgl = $this->getMailboxGroupListParser()->parse($data);
		
		foreach($mgl as $group)
		{
			/** @var MailboxGroupInterface $group */
			return $group;
		}
		
		return new MailboxGroup(new MailboxList([]));
	}
	
}
