<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use Iterator;
use PhpExtended\Domain\DomainInterface;

/**
 * MailboxGroup class file.
 * 
 * This class is a simple implementation of the MailboxGroupInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class MailboxGroup implements MailboxGroupInterface
{
	
	/**
	 * The display name of the group.
	 * 
	 * @var ?string
	 */
	protected ?string $_displayName = null;
	
	/**
	 * The mailbox list of this group.
	 * 
	 * @var MailboxListInterface
	 */
	protected MailboxListInterface $_mailboxList;
	
	/**
	 * Builds a new MailboxGroup with the given email address and display name.
	 * 
	 * @param MailboxListInterface $list
	 * @param ?string $display
	 */
	public function __construct(MailboxListInterface $list, ?string $display = null)
	{
		$this->_mailboxList = $list;
		$this->_displayName = $display;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getCanonicalRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::getDisplayName()
	 */
	public function getDisplayName() : string
	{
		return (string) $this->_displayName;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return $this->_mailboxList->count();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::current()
	 */
	public function current() : MailboxInterface
	{
		return $this->_mailboxList->current();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		$this->_mailboxList->next();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return (int) $this->_mailboxList->key();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return $this->_mailboxList->valid();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		$this->_mailboxList->rewind();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::withDomain()
	 */
	public function withDomain(DomainInterface $domain) : MailboxGroupInterface
	{
		return new MailboxGroup($this->_mailboxList->withDomain($domain), $this->_displayName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::filterByDomain()
	 */
	public function filterByDomain(DomainInterface $domain) : MailboxGroupInterface
	{
		return new MailboxGroup($this->_mailboxList->filterByDomain($domain), $this->_displayName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::withDisplayName()
	 */
	public function withDisplayName(string $display) : MailboxGroupInterface
	{
		return new MailboxGroup($this->_mailboxList, $display);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::withEmailAddress()
	 */
	public function withEmailAddress(EmailAddressInterface $email, ?string $display = null) : MailboxGroupInterface
	{
		return new MailboxGroup($this->_mailboxList->withEmailAddress($email, $display), $this->_displayName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::withEmailAddressList()
	 */
	public function withEmailAddressList(EmailAddressListInterface $addressList) : MailboxGroupInterface
	{
		return new MailboxGroup($this->_mailboxList->withEmailAddressList($addressList), $this->_displayName);
	}
	
	/**
	 * Adds a mailbox to this list.
	 *
	 * @param MailboxInterface $mailbox
	 * @return MailboxGroupInterface
	 */
	public function withMailbox(MailboxInterface $mailbox) : MailboxGroupInterface
	{
		return new MailboxGroup($this->_mailboxList->withMailbox($mailbox), $this->_displayName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::withMailboxList()
	 */
	public function withMailboxList(MailboxListInterface $mailboxList) : MailboxGroupInterface
	{
		return new MailboxGroup($this->_mailboxList->withMailboxList($mailboxList), $this->_displayName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::getMailboxList()
	 */
	public function getMailboxList() : MailboxListInterface
	{
		return $this->_mailboxList;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxList::collectDisplayNames()
	 */
	public function collectDisplayNames() : Iterator
	{
		return $this->_mailboxList->collectDisplayNames();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return $this->_mailboxList->isEmpty();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::collectDomains()
	 */
	public function collectDomains() : Iterator
	{
		return $this->_mailboxList->collectDomains();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::collectEmailAddresses()
	 */
	public function collectEmailAddresses() : EmailAddressListInterface
	{
		return $this->_mailboxList->collectEmailAddresses();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxList::getCanonicalRepresentation()
	 */
	public function getCanonicalRepresentation() : string
	{
		if(null === $this->_displayName || '' === $this->_displayName)
		{
			return $this->_mailboxList->getCanonicalRepresentation();
		}
		
		$display = $this->_displayName;
		if(!\preg_match('#^[A-Za-z0-9 _+-]+$#', $display))
		{
			$display = \strtr($display, ['\\' => '\\\\']);
			$display = \strtr($display, ['"' => '\\"']);
			$display = '"'.$display.'"';
		}
		
		return $display.': '.$this->_mailboxList->getCanonicalRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxList::equals()
	 */
	public function equals($other) : bool
	{
		return $other instanceof MailboxGroupInterface
			&& $this->getDisplayName() === $other->getDisplayName()
			&& $this->_mailboxList->equals($other->getMailboxList());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::containsEmailAddress()
	 */
	public function containsEmailAddress(EmailAddressInterface $address) : bool
	{
		return $this->_mailboxList->containsEmailAddress($address);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::containsEmailAddressList()
	 */
	public function containsEmailAddressList(EmailAddressListInterface $addressList) : bool
	{
		return $this->_mailboxList->containsEmailAddressList($addressList);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::containsMailbox()
	 */
	public function containsMailbox(MailboxInterface $mailbox) : bool
	{
		return $this->_mailboxList->containsMailbox($mailbox);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::containsMailboxIgnoreLabels()
	 */
	public function containsMailboxIgnoreLabels(MailboxInterface $mailbox) : bool
	{
		return $this->_mailboxList->containsMailboxIgnoreLabels($mailbox);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::containsMailboxList()
	 */
	public function containsMailboxList(MailboxListInterface $mailboxList) : bool
	{
		return $this->_mailboxList->containsMailboxList($mailboxList);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::containsMailboxListIgnoreLabels()
	 */
	public function containsMailboxListIgnoreLabels(MailboxListInterface $mailboxList) : bool
	{
		return $this->_mailboxList->containsMailboxListIgnoreLabels($mailboxList);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::containsMailboxGroup()
	 */
	public function containsMailboxGroup(MailboxGroupInterface $mailboxGroup) : bool
	{
		return $this->getDisplayName() === $mailboxGroup->getDisplayName()
			&& $this->containsMailboxList($mailboxGroup->getMailboxList());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxGroupInterface::containsMailboxGroupIgnoreLabels()
	 */
	public function containsMailboxGroupIgnoreLabels(MailboxGroupInterface $mailboxGroup) : bool
	{
		return $this->containsMailboxListIgnoreLabels($mailboxGroup->getMailboxList());
	}
	
}
