<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use PhpExtended\Domain\DomainInterface;

/**
 * Mailbox class file.
 * 
 * This class is a simple implementation of the MailboxInterface.
 * 
 * @author Anastaszor
 */
class Mailbox implements MailboxInterface
{
	
	/**
	 * The display name of the mailbox.
	 * 
	 * @var ?string
	 */
	protected ?string $_displayName = null;
	
	/**
	 * The email of the mailbox.
	 * 
	 * @var EmailAddressInterface
	 */
	protected EmailAddressInterface $_email;
	
	/**
	 * Builds a new Mailbox with the given email address and display name.
	 * 
	 * @param EmailAddressInterface $email
	 * @param string $display
	 */
	public function __construct(EmailAddressInterface $email, ?string $display = null)
	{
		$this->_email = $email;
		if(null !== $display)
		{
			$this->_displayName = (string) \preg_replace('#\\\\(.)#', '$1', $display);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getCanonicalRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxInterface::getDisplayName()
	 */
	public function getDisplayName() : string
	{
		return (string) $this->_displayName;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxInterface::withDisplayName()
	 */
	public function withDisplayName(string $displayName) : MailboxInterface
	{
		return new Mailbox($this->_email, (string) \preg_replace('#\\\\(.)#', '$1', $displayName));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxInterface::getEmailAddress()
	 */
	public function getEmailAddress() : EmailAddressInterface
	{
		return $this->_email;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxInterface::withEmailAddress()
	 */
	public function withEmailAddress(EmailAddressInterface $email) : MailboxInterface
	{
		return new Mailbox($email, $this->_displayName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxInterface::withEmailAddressLocalPart()
	 */
	public function withEmailAddressLocalPart(string $local) : MailboxInterface
	{
		return new Mailbox($this->_email->withLocalPart($local), $this->_displayName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxInterface::withEmailAddressDomain()
	 */
	public function withEmailAddressDomain(DomainInterface $domain) : MailboxInterface
	{
		return new Mailbox($this->_email->withDomain($domain), $this->_displayName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxInterface::getCanonicalRepresentation()
	 */
	public function getCanonicalRepresentation() : string
	{
		if(null === $this->_displayName || '' === $this->_displayName)
		{
			return $this->_email->getCanonicalRepresentation();
		}
		
		$display = $this->_displayName;
		if(!\preg_match('#^[A-Za-z0-9 _+-]+$#', $display))
		{
			$display = (string) \strtr($display, ['\\' => '\\\\']);
			$display = (string) \strtr($display, ['"' => '\\"']);
			$display = '"'.$display.'"';
		}
		
		return $display.' <'.$this->_email->getCanonicalRepresentation().'>';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxInterface::equals()
	 */
	public function equals($other) : bool
	{
		return $other instanceof MailboxInterface
			&& $this->getDisplayName() === $other->getDisplayName()
			&& $this->containsEmailAddress($other->getEmailAddress());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxInterface::containsEmailAddress()
	 */
	public function containsEmailAddress(EmailAddressInterface $address) : bool
	{
		return $this->getEmailAddress()->equals($address);
	}
	
}
