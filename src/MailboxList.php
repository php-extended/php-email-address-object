<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use ArrayIterator;
use InvalidArgumentException;
use Iterator;
use PhpExtended\Domain\DomainInterface;

/**
 * MailboxList class file.
 * 
 * This class is a simple implementation of the MailboxListInterface with
 * anti duplication of email addresses.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class MailboxList implements MailboxListInterface
{
	
	/**
	 * The store of mailboxes.
	 * 
	 * @var array<integer, MailboxInterface>
	 */
	protected array $_mailboxes = [];
	
	/**
	 * Builds a new Mailbox List with the given array of mailbox addresses.
	 * 
	 * @param array<integer, MailboxInterface> $list
	 */
	public function __construct(array $list = [])
	{
		foreach($list as $mailbox)
		{
			$this->addMailbox($mailbox);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getCanonicalRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_mailboxes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::current()
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function current() : MailboxInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return \current($this->_mailboxes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		\next($this->_mailboxes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return (int) \key($this->_mailboxes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return false !== \current($this->_mailboxes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		\reset($this->_mailboxes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::withDomain()
	 * @throws InvalidArgumentException
	 */
	public function withDomain(DomainInterface $domain) : MailboxListInterface
	{
		$list = [];
		
		foreach($this->_mailboxes as $mailbox)
		{
			$list[] = $mailbox->withEmailAddressDomain($domain);
		}
		
		return new MailboxList($list);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::filterByDomain()
	 */
	public function filterByDomain(DomainInterface $domain) : MailboxListInterface
	{
		$canonical = $domain->getCanonicalRepresentation();
		$list = [];
		
		foreach($this->_mailboxes as $mailbox)
		{
			if($mailbox->getEmailAddress()->getDomain()->getCanonicalRepresentation() === $canonical)
			{
				$list[] = $mailbox;
			}
		}
		
		return new MailboxList($list);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::withEmailAddress()
	 */
	public function withEmailAddress(EmailAddressInterface $email, ?string $display = null) : MailboxListInterface
	{
		return $this->withMailbox(new Mailbox($email, $display));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::withEmailAddresses()
	 */
	public function withEmailAddressList(EmailAddressListInterface $addressList) : MailboxListInterface
	{
		$mbl = new MailboxList($this->_mailboxes);
		
		foreach($addressList as $address)
		{
			$mbl->addMailbox(new Mailbox($address));
		}
		
		return $mbl;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::withMailbox()
	 */
	public function withMailbox(MailboxInterface $mailbox) : MailboxListInterface
	{
		return (new MailboxList($this->_mailboxes))->addMailbox($mailbox);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::withMailboxList()
	 */
	public function withMailboxList(MailboxListInterface $mailboxList) : MailboxListInterface
	{
		$mbl = new MailboxList($this->_mailboxes);
		
		foreach($mailboxList as $mailbox)
		{
			$mbl->addMailbox($mailbox);
		}
		
		return $mbl;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::getCanonicalRepresentation()
	 */
	public function getCanonicalRepresentation() : string
	{
		$str = [];
		
		foreach($this->_mailboxes as $mailbox)
		{
			$str[] = $mailbox->getCanonicalRepresentation();
		}
		
		return \implode(', ', $str);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressListInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return empty($this->_mailboxes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::collectDisplayNames()
	 */
	public function collectDisplayNames() : Iterator
	{
		$displays = [];
		
		foreach($this->_mailboxes as $mailbox)
		{
			$displays[] = $mailbox->getDisplayName();
		}
		
		return new ArrayIterator($displays);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::collectDomains()
	 */
	public function collectDomains() : Iterator
	{
		$domains = [];
		
		foreach($this->_mailboxes as $mailbox)
		{
			$domains[$mailbox->getEmailAddress()->getDomain()->getCanonicalRepresentation()] = $mailbox->getEmailAddress()->getDomain();
		}
		
		return new ArrayIterator(\array_values($domains));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::collectEmailAddresses()
	 */
	public function collectEmailAddresses() : EmailAddressListInterface
	{
		$emails = [];
		
		foreach($this->_mailboxes as $mailbox)
		{
			$emails[] = $mailbox->getEmailAddress();
		}
		
		return new EmailAddressList($emails);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::equals()
	 */
	public function equals($other) : bool
	{
		if(!$other instanceof MailboxListInterface)
		{
			return false;
		}
		
		if($this->count() !== $other->count())
		{
			return false;
		}
		
		return $this->containsMailboxList($other)
			&& $other->containsMailboxList($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::containsEmailAddress()
	 */
	public function containsEmailAddress(EmailAddressInterface $address) : bool
	{
		foreach($this->_mailboxes as $mailbox)
		{
			if($mailbox->containsEmailAddress($address))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::containsEmailAddressList()
	 */
	public function containsEmailAddressList(EmailAddressListInterface $addressList) : bool
	{
		foreach($addressList as $address)
		{
			if(!$this->containsEmailAddress($address))
			{
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::containsMailbox()
	 */
	public function containsMailbox(MailboxInterface $mailbox) : bool
	{
		foreach($this->_mailboxes as $thismailbox)
		{
			if($thismailbox->equals($mailbox))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::containsMailboxIgnoreLabels()
	 */
	public function containsMailboxIgnoreLabels(MailboxInterface $mailbox) : bool
	{
		foreach($this->_mailboxes as $thismailbox)
		{
			if($thismailbox->containsEmailAddress($mailbox->getEmailAddress()))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::containsMailboxList()
	 */
	public function containsMailboxList(MailboxListInterface $mailboxList) : bool
	{
		foreach($mailboxList as $mailbox)
		{
			if(!$this->containsMailbox($mailbox))
			{
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxListInterface::containsMailboxListIgnoreLabels()
	 */
	public function containsMailboxListIgnoreLabels(MailboxListInterface $mailboxList) : bool
	{
		foreach($mailboxList as $mailbox)
		{
			if(!$this->containsMailboxIgnoreLabels($mailbox))
			{
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Adds the given mailbox to this list.
	 * 
	 * @param MailboxInterface $mailbox
	 * @return MailboxListInterface
	 */
	protected function addMailbox(MailboxInterface $mailbox) : MailboxListInterface
	{
		foreach($this->_mailboxes as $current)
		{
			if($current->getEmailAddress()->equals($mailbox->getEmailAddress()))
			{
				return $this; // already present, anti duplicate
			}
		}
		$this->_mailboxes[] = $mailbox;
		
		return $this;
	}
	
}
