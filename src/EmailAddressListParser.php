<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-email-address-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Email;

use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;

/**
 * EmailAddressListParser class file.
 * 
 * This class is a simple implementation of the EmailAddressListParserInterface.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\AbstractParserLexer<EmailAddressListInterface>
 */
class EmailAddressListParser extends AbstractParserLexer implements EmailAddressListParserInterface
{
	
	public const L_DQUOT = 1; // "
	public const L_ASLSH = 2; // \
	public const L_COMMA = 3; // ,
	
	public const T_QUSTR_PART = 10; // "abc
	public const T_QUSTR_END = 11;  // "abc"
	public const T_QUSTR_SLSH = 12; // "abc\
	public const T_UNQTD_PART = 15; // abc OR ab"abc"de
	
	/**
	 * The email address parser.
	 * 
	 * @var ?EmailAddressParserInterface
	 */
	protected $_emailAddressParser;
	
	/**
	 * Builds a new EmailAddressListParser on top of an email address parser
	 * if given.
	 * 
	 * @param EmailAddressParserInterface $emailAddressParser
	 */
	public function __construct(?EmailAddressParserInterface $emailAddressParser = null)
	{
		parent::__construct(EmailAddressListInterface::class);
		$this->_emailAddressParser = $emailAddressParser;
		
		$this->_config->addMappings('"', self::L_DQUOT);
		$this->_config->addMappings('\\', self::L_ASLSH);
		$this->_config->addMappings(',', self::L_COMMA);
		
		$this->_config->addMerging(LexerInterface::L_TRASH, LexerInterface::L_TRASH, self::T_UNQTD_PART);
		$this->_config->addMerging(self::T_UNQTD_PART, self::L_ASLSH, self::T_UNQTD_PART);
		$this->_config->addMerging(self::T_UNQTD_PART, self::L_DQUOT, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_UNQTD_PART, LexerInterface::L_TRASH, self::T_UNQTD_PART);
		
		$this->_config->addMerging(LexerInterface::L_TRASH, self::L_DQUOT, self::T_QUSTR_PART);
		$this->_config->addMerging(self::L_DQUOT, LexerInterface::L_TRASH, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_PART, LexerInterface::L_TRASH, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_PART, self::L_DQUOT, self::T_QUSTR_END);
		$this->_config->addMerging(self::T_QUSTR_PART, self::L_ASLSH, self::T_QUSTR_SLSH);
		$this->_config->addMerging(self::T_QUSTR_PART, self::L_COMMA, self::T_QUSTR_PART);
		
		$this->_config->addMerging(self::T_QUSTR_SLSH, LexerInterface::L_TRASH, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_SLSH, self::L_DQUOT, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_SLSH, self::L_ASLSH, self::T_QUSTR_PART);
		$this->_config->addMerging(self::T_QUSTR_SLSH, self::L_COMMA, self::T_QUSTR_PART);
		
		$this->_config->addMerging(self::T_QUSTR_END, LexerInterface::L_TRASH, self::T_UNQTD_PART);
	}
	
	public function getEmailAddressParser() : EmailAddressParserInterface
	{
		if(null === $this->_emailAddressParser)
		{
			$this->_emailAddressParser = new EmailAddressParser();
		}
		
		return $this->_emailAddressParser;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\EmailAddressParserInterface::parseString()
	 */
	public function parse(?string $data) : EmailAddressListInterface
	{
		return parent::parse($data); // interface compatibility
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Email\MailboxParserInterface::parseString()
	 */
	public function parseLexer(LexerInterface $lexer) : EmailAddressListInterface
	{
		$lexer->rewind();
		$addresses = [];
		$token = null;
		$data = '';
		
		while($lexer->valid())
		{
			$token = $this->expectOneOf($lexer, [
				LexerInterface::L_TRASH,
				self::T_QUSTR_PART,
				self::T_QUSTR_END,
				self::T_QUSTR_SLSH,
				self::T_UNQTD_PART,
				self::L_COMMA,
			], $token);
			
			if($token->getCode() === self::L_COMMA)
			{
				$data = \trim($data);
				if('' !== $data)
				{
					$addresses[] = $this->getEmailAddressParser()->parse($data);
				}
				$data = '';
				continue;
			}
			
			$data .= $token->getData();
		}
		
		if('' !== $data)
		{
			$data = \trim($data);
			if('' !== $data)
			{
				$addresses[] = $this->getEmailAddressParser()->parse($data);
			}
		}
		
		return new EmailAddressList($addresses);
	}
	
}
