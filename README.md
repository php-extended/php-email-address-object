# php-extended/php-email-address-object

A library that implements the php-extended/php-email-address-interface library.

![coverage](https://gitlab.com/php-extended/php-email-address-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-email-address-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-email-address-object ^8`


## Basic Usage

To use this interface, you may do the following :

```php

use PhpExtended\Domain\Domain;
use PhpExtended\Email\EmailAddress;
use PhpExtended\Email\Mailbox;
use PhpExtended\Email\MailboxGroup;
use PhpExtended\Email\MailboxGroupList;

$email = new EmailAddress('example', new Domain('example.com')); // example@example.com
$email->__toString(); // example@example.com

$mailbox = new Mailbox($email, 'Example, Inc');
$mailbox->__toString(); // "Example, Inc" <example@example.com>

$mailboxList = new MailboxList([$mailbox]);
$mailboxList->__toString(); // "Example, Inc" <example@example.com>

$mailboxGroup = new MailboxGroup($mailboxList, 'All Examples');
$mailboxGroup->__toString(); // "All Examples": "Example, Inc" <example@example.com>

$mailboxGroupList = new MailboxGroupList([$mailboxGroup]);
$mailboxGroupList = $mailboxGroupList->withEmailAddress(new EmailAddress('example2', 'example.com'), 'Example2, Inc', 'Other Examples');
$mailboxGroupList->__toString(); // "All Examples": "Example, Inc" <example@example.com>; "Other Examples": "Example2, Inc" <example2@example.com>

$emails = $mailboxGroupList->collectEmailAddresses();
echo implode(', ', iterator_to_array($emails)); // echo "example@example.com, example2@example.com"

```

To parse email addresses, you may do the following :

```php

use PhpExtended\Email\EmailAddressParser;

$parser = new EmailAddressParser();
$email = $parser->parse('email@example.com');
// $email instanceof \PhpExtended\Email\EmailAddress

```

or, for headers of requests :

```php

use PhpExtended\Email\MailboxGroupListParser;

$parser = new MailboxGroupListParser();
$list = $parser->parse('Group: Toto toto@example.com; Group2: toto2@example.com, "Tata \"" tata@example.com');
// $list instanceof \PhpExtended\Email\MailboxGroupList
// Group :
//    Toto <toto@example.com>
// Group 2:
//         <toto2@example.com>
//    Tata " <tata@example.com> // <- The display name is 'Tata "' and is properly quoted and escaped on canonicalRepresentation

echo $list->collectEmailAddresses();
// echoes "toto@example.com, toto2@example.com, tata@example.com"

```


## Test data

This library was tested against the following data sets:
- [https://gist.github.com/cjaoude/fd9910626629b53c4d25](https://gist.github.com/cjaoude/fd9910626629b53c4d25)
- [https://github.com/dominicsayers/isemail/blob/master/test/tests.xml](https://github.com/dominicsayers/isemail/blob/master/test/tests.xml)
- [https://en.wikipedia.org/wiki/Email_address](https://en.wikipedia.org/wiki/Email_address)

If you can find other data sets to test against, just file an issue and i will try
to test this library against it.


## License

MIT (See [license file](LICENSE)).
